/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

/* -*-             c-basic-offset: 4; indent-tabs-mode: nil; -*-  //------100-columns-wide------>|*/
// for license see accompanying LICENSE_TESTS.txt file (available also at http://www.xmlpull.org)

package xmlpull.tests;

import utest.Assert;

//


import haxe.io.StringInput;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;
import xmlpull.XmlConstants;

/**
 * Simple test for CDATA parsing.
 *
 * @author <var href : a="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestCdsect
{
    private var factory : XmlPullParserFactory;
    
    public function new()
    {
    }
    
    private function setup() : Void
    {
        factory = Util.newFactory();
        factory.setFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES, true);
        Assert.equals(true, factory.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
    }
    
    private function teardown() : Void
    {
    }

    
    public function testCdsect() : Void
    {
        var xpp : XmlPullParser = factory.newPullParser();
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        
        var XML = "<t><![CDATA[ f]]>o<![CDATA[o ]]></t>";
        
        xpp.setInput(new StringInput(XML));
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "t", null, false/*empty*/, 0);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null, " foo ", false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "t", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
    }
    
    public function testCdsectWithEol() : Void
    {
        var xpp : XmlPullParser = factory.newPullParser();
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        
        var XML = "<t> \n<![CDATA[fo]]>o<![CDATA[ \r\n\r]]>\n</t>";
        
        xpp.setInput(new StringInput(XML));
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "t", null, false/*empty*/, 0);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null, " \nfoo \n\n\n", false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "t", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
    }
    
    public function testCdsectWithComment() : Void
    {
        var xpp : XmlPullParser = factory.newPullParser();
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        
        var XML = "<t> \n<![CDATA[foo<![CDATA[ \r\n\r]]-->]]>\r</t>";
        
        xpp.setInput(new StringInput(XML));
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "t", null, false/*empty*/, 0);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null, " \nfoo<![CDATA[ \n\n]]-->\n", false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "t", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
    }
    
    public function testCdsectSoleContent() : Void
    {
        var xpp : XmlPullParser = factory.newPullParser();
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        
        var XML = "<t><![CDATA[foo<![CDATA[ \r\n\r]]-->]]></t>";
        
        xpp.setInput(new StringInput(XML));
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "t", null, false/*empty*/, 0);
        xpp.next();
        //System.err.println(getClass()+" text="+Util.printable(xpp.getText()));
        Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null, "foo<![CDATA[ \n\n]]-->", false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "t", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
    }

    
    public function testBigCdsects() : Void
    {
        checkBigCdsect(1);
        checkBigCdsect(10);
        checkBigCdsect(15);
        checkBigCdsect(16);
        checkBigCdsect(100);
        checkBigCdsect(127);
        checkBigCdsect(128);
        checkBigCdsect(1023);
        checkBigCdsect(1024);
        checkBigCdsect(1025);
        checkBigCdsect(16*1024-1);
        checkBigCdsect(16*1024);
        checkBigCdsect(16*1024+1);
    }
    
    public function checkBigCdsect(size:Int) : Void
    {
        var xpp : XmlPullParser = factory.newPullParser();
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        
        // inspired by http://www.extreme.indiana.edu/bugzilla/show_bug.cgi?id=238
        var cdataContent : StringBuf = new StringBuf();
        for (i in 0...size)
        {
            cdataContent.addChar((i % 10) + "0".code);
        }

        var cdataStr = cdataContent.toString();
        
        var XML : StringBuf = new StringBuf();
        XML.add("<foo><![CDATA[");
        XML.add(cdataStr);
        XML.add("]]></foo>");
        
        xpp.setInput(new StringInput(XML.toString()));
        
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "foo", null, false/*empty*/, 0);
        xpp.next();
        //System.err.println(getClass()+" text="+Util.printable(xpp.getText()+" expected="+cdataContent.toString()));
        Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null, cdataStr, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "foo", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
    
    }
}

