/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

/* -*-             c-basic-offset: 4; indent-tabs-mode: nil; -*-  //------100-columns-wide------>|*/
// for license see accompanying LICENSE_TESTS.txt file (available also at http://www.xmlpull.org)


/*
 SYNTAX: (// marks unimplemented)
 <tests xmlns="http://xmlpull.org/v1/tests/2002-08.xsd">
 <test-parser name="descriptive name of test">
 <input-inline>inlined xml</input-inline>?
// <input-file>name of file</input-file>?
 <set-feature>name of feature</set-feature>*
 <expect
 type="START_DOCUMENT|END_DOCUMENT|START_TAG|END_TAG|TEXT|COMMENT|..."?
 name="getName()"? namespace="getNamespace()"?
// prefix="getPrefix()"? depth="getDepth()"? empty="isEmptyElementTag"?
// namespaceCount="getNamespacesCount(getDepth())"? attributeCount="getAttributeCount()"?
// text="getText()" isWhitespace="isWhitespace():
 />*
// <check-attribute pos="position" prefix="getAttributePrefix(pos)"? namespace=...? name=...?
// value="..." is-default="0|1"?/>
// <check-namespace pos="position" prefix="getNamespacePrefix(pos)"? namespace=...?/>
 <next>*
// <nextToken>*
// <nextTag>*
// <nextText>*
// <!-- what about require(), getTextCharacters(), getLineNumber/getColumnNumber(), defineEntityReplacementText()?/>
 </test-parser>*
// <test-serializer>
// <start-tag name="..." namespace="..."/>
// </test-serializer>
 </tests>
 */

package xmlpull.tests;

import utest.Assert;

import haxe.io.StringInput;
import haxe.io.Input;
import sys.io.File;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserException;
import xmlpull.XmlPullParserFactory;
//import org.xmlpull.v1.util.XmlPullWrapper;

//import org.xmlpull.v1.tests.UtilTestCase;

class XmlTestDef
{
    inline private static var NS_URI = "http://xmlpull.org/v1/tests/2002-08.xsd";
    //private static boolean standalone = true;
    inline private static var isVerbose : Bool = false;
    
    //public static void main (String[] args) {
    //    junit.textui.TestRunner.run (new TestSuite(XmlTestCase.class));
    //}
    
    private function new()
    {
        
        //standalone = false;
    }
    
    private static function verbose(msg:String) : Void
    {
      if (isVerbose) trace(msg);
    }
    
    private static function info(msg:String) : Void
    {
    }
    
    // FIXME hardcoded path
    private static inline var TESTS_PATH = "/Users/jsz/dev/parensoft/xml/xmlpull-tests/res/";
    
    private static function openStream(name:String) : Input
    {
        return File.read(TESTS_PATH + name);
    }
    /**
     *
     */
    public static function testXml(testName:String) : Void
        
        {
        
        var factory : XmlPullParserFactory = Util.newFactory();
        factory.setNamespaceAware(true);
        //XmlPullWrapper wrapper;

        
        var pp : XmlPullParser = factory.newPullParser();
        //wrapper = new XmlPullWrapper(pp);

        
        var is = openStream(testName);
        verbose("> LOADING TESTS '"+testName+"'");
        pp.setInputWithEncoding(is, null);
        executeTests(pp);
        is.close();
        verbose("< FINISHED TESTS '"+testName+"'");
    }

    
    private static function executeTests(pp:XmlPullParser) : Void
    {
        
        //wrapper.nextStartTag(NS_URI, "tests");
        pp.nextTag();
        pp.require(XmlEventType.START_TAG, NS_URI, "tests");
        while(pp.nextTag() == XmlEventType.START_TAG)
        {
            executeTestParser(pp);
        }
        pp.require(XmlEventType.END_TAG, NS_URI, "tests");
    }
    
    private static function executeTestParser(pp:XmlPullParser) : Void
    {
        pp.require(XmlEventType.START_TAG, NS_URI, "test-parser");
        var testName : String = pp.getAttributeValue("", "name");
        verbose(">> START TEST '"+testName+"'");
        //testFactory = XmlPullParserFactory.newInstance();
        var testFactory : XmlPullParserFactory = Util.newFactory();
        verbose(">> using testFactory='"+Type.getClass(testFactory)+"'");
        var testParser : XmlPullParser = null;
        var testInputReady : Bool = false;
        while(pp.nextTag() == XmlEventType.START_TAG)
        {
            var action : String = pp.getName();
            if("create-parser" == (action)) {
                testParser = testFactory.newPullParser();
                verbose(">> using testParser='"+Type.getClass(testParser)+"'");
                pp.nextText();
            } else if("input-inline" == (action)) {
                if(testInputReady)
                {
                    throw ("input already set"+pp.getPositionDescription());
                }
                testInputReady = true;
                var input : String = pp.nextText();
                verbose(">>> "+action+" to '"+Util.printable(input)+"'");
                verbose(">>> "+action+" to '"+input+"'");
                testParser.setInput(new StringInput( input));
            } else if("expect" == (action)) {
                var type : String = pp.getAttributeValue("", "type");
                var namespace : String = pp.getAttributeValue("", "namespace");
                var name : String = pp.getAttributeValue("", "name");
                var empty : String = pp.getAttributeValue("", "empty");
                verbose(">>> "+action
                            +" type="+type
                            +(namespace != null ? " namespace="+namespace : "")
                            +(name != null ? " name="+name : "")
                            +(empty != null ? " empty="+empty : ""));
                if(type != null)
                {
                    var event = XmlEventType.forName(type);
                    // comapring string give better error messages
                    Assert.equals(event.toString(), testParser.getEventType().toString());
                    // now compare actual int values
                    Assert.equals(event, testParser.getEventType());
                }
                if(namespace != null) Assert.equals(namespace, testParser.getNamespace());
                if(name != null) Assert.equals(name, testParser.getName());
                if(empty != null)
                {
                    var isEmpty : Bool = (empty.toLowerCase() == "true");
                    Assert.equals(isEmpty, testParser.isEmptyElementTag());
                }
                pp.nextText();
            } else if("next" == (action)) {
                verbose(">>> "+action);
                testParser.next();
                pp.nextText();
            } else if("next-tag" == (action)) {
                verbose(">>> "+action);
                testParser.nextTag();
                pp.nextText();
            } else if("next-text" == (action)) {
                verbose(">>> "+action); //+" on "+testParser.getPositionDescription()+"");
                var expected : String = pp.getAttributeValue("", "text");
                var testText : String = testParser.nextText();
                if(expected != null)
                {
                    verbose(">>> "+action+" testParser="+testParser.getPositionDescription()
                           +" excpectecd='"+Util.printable(expected)+"' test='"+Util.printable(testText)+"'");
                    Assert.equals(Util.printable(expected), Util.printable(testText), testParser.getPositionDescription());
                    Assert.equals(expected, testText);
                }
                pp.nextText();
            } else if("set-feature" == (action)) {
                var feature : String = pp.nextText();
                verbose(">>> "+action+" "+feature);
                testParser.setFeature(feature, true);
            } else
            {
                verbose(">>> UNKNONW ACTION '"+action+"' ("+pp.getPositionDescription()+")");
                var content : String = pp.nextText();
                if(content.length > 0)
                {
                    verbose(">>> CONTENT='"+Util.printable(content)+"'");
                }
            
            }
        }
        
        pp.require(XmlEventType.END_TAG, NS_URI, "test-parser");
        verbose(">> END TEST '"+testName+"'");
    }

    
}

