/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

/* -*-             c-basic-offset: 4; indent-tabs-mode: nil; -*-  //------100-columns-wide------>|*/
// for license see accompanying LICENSE_TESTS.txt file (available also at http://www.xmlpull.org)

package xmlpull.tests;

import utest.Assert;



import haxe.io.StringInput;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;

import xmlpull.XmlConstants.*;

/**
 * Tests checking miscellaneous features.
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestMisc 
{
    private var factory : XmlPullParserFactory;
    
    public function new()
    {
        
    }
    
    private function setup() : Void
    {
        factory = Util.newFactory();
        factory.setFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES, true);
        Assert.equals(true, factory.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
    }
    
    private function teardown() : Void
    {
    }
    
    public function testCharactersLegacy() : Void
    {
        // [14]         CharData         ::=    [^<&]* - ([^<&]* ']]>' [^<&]*)
        // SGML "legacy" in XML 1.0 as described in http://www.w3.org/TR/REC-xml#syntax
        // that XML parsers are required to throw error if they encounter ']]>'
        var INPUT_XML = "<t>  <test1>d] ]] > ]> fd</test1>\n<test2>]]>  </test2>\n </t>";
        var pp : XmlPullParser = factory.newPullParser();
        pp.setInput( new StringInput( INPUT_XML));
        pp.nextTag();
        pp.require( XmlEventType.START_TAG, null, "t");
        pp.nextTag();
        pp.require( XmlEventType.START_TAG, null, "test1");
        Assert.equals( "d] ]] > ]> fd", pp.nextText());
        pp.require( XmlEventType.END_TAG, null, "test1");
        
        pp.nextTag();
        pp.require( XmlEventType.START_TAG, null, "test2");
        try
        {
            pp.nextText();
            Assert.fail("if TEXT contains ]]> parser must fail");
        } catch(ex:XmlPullParserException) {}
    }

   
   public function testNextTag() : Void
   {
        var INPUT_XML = "<t>  <test1>foo</test1>\n<test2>  </test2>\n </t>";
        var pp : XmlPullParser = factory.newPullParser();
        pp.setInput( new StringInput( INPUT_XML));
        pp.nextTag();
        pp.require( XmlEventType.START_TAG, null, "t");
        pp.nextTag();
        pp.require( XmlEventType.START_TAG, null, "test1");
        Assert.equals( "foo", pp.nextText());
        pp.require( XmlEventType.END_TAG, null, "test1");
        
        pp.nextTag();
        pp.require( XmlEventType.START_TAG, null, "test2");
        pp.nextTag();
        pp.require( XmlEventType.END_TAG, null, "test2");

        
        pp.nextTag();
        pp.require( XmlEventType.END_TAG, null, "t");
        pp.next();
        pp.require( XmlEventType.END_DOCUMENT, null, null);
    
    }

    
    //    public void testReadText() throws Exception {
    //        final String INPUT_XML = "<test>foo</test>";
    //        XmlPullParser pp = factory.newPullParser();
    //        pp.setInput( new StringReader( INPUT_XML ) );
    //        assertEquals( "", pp.readText() );
    //        pp.next();
    //        assertEquals( "", pp.readText() );
    //        pp.next();
    //        assertEquals( "foo", pp.readText() );
    //        assertEquals( pp.TYPES[ XmlPullParser.END_TAG ], pp.TYPES[ pp.getEventType() ]);
    //    }
    
    public function testNextText() : Void
    {
        var INPUT_XML = "<t><test1>foo</test1><test2></test2><test3/><test4>bar</test4></t>";
        var pp : XmlPullParser = factory.newPullParser();
        pp.setInput( new StringInput( INPUT_XML));
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "t");
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "test1");
        Assert.equals( "foo", pp.nextText());
        pp.require( XmlEventType.END_TAG, null, "test1");
        
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "test2");
        Assert.equals( "", pp.nextText());
        pp.require( XmlEventType.END_TAG, null, "test2");
        
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "test3");
        Assert.equals( "", pp.nextText());
        pp.require( XmlEventType.END_TAG, null, "test3");
        
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "test4");
        //pp.next();
        //pp.require( XmlPullParser.TEXT, null, null);
        Assert.equals( "bar", pp.nextText());
        pp.require( XmlEventType.END_TAG, null, "test4");
        
        pp.next();
        pp.require( XmlEventType.END_TAG, null, "t");
        pp.next();
        pp.require( XmlEventType.END_DOCUMENT, null, null);
        
        // now check for error conditions
        pp.setInput( new StringInput( INPUT_XML));
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "t");
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "test1");
        pp.next();
        pp.require( XmlEventType.TEXT, null, null);
        try
        {
            pp.nextText();
            Assert.fail("if current tag is TEXT no next text content can be returned!");
        } catch(ex:XmlPullParserException) {}
        
        pp.setInput( new StringInput( INPUT_XML));
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "t");
        try
        {
            pp.nextText();
            Assert.fail("if next tag is START_TAG no text content can be returned!");
        } catch(ex:XmlPullParserException) {}
        
        pp.setInput( new StringInput( INPUT_XML));
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "t");
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "test1");
        pp.next();
        pp.next();
        pp.require( XmlEventType.END_TAG, null, "test1");
        try
        {
            pp.nextText();
            Assert.fail("if current tag is END_TAG no text content can be returned!");
        } catch(ex:XmlPullParserException) {}
    
    }
    
    public function testRequire() : Void
    {
        //public void require (int type, String namespace, String name)
        var INPUT_XML = "<test><t>foo</t><m:s xmlns:m='URI'>\t</m:s></test>";
        var pp : XmlPullParser = factory.newPullParser();
        pp.setInput( new StringInput( INPUT_XML));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "test");
        pp.require( XmlEventType.START_TAG, "", null);
        pp.require( XmlEventType.START_TAG, "", "test");
        pp.next();
        pp.require( XmlEventType.START_TAG, "", "t");
        pp.next();
        pp.require( XmlEventType.TEXT, null, null);
        pp.next();
        pp.require( XmlEventType.END_TAG, "", "t");
        
        pp.next();
        pp.require( XmlEventType.START_TAG, "URI", "s");
        
        pp.next();
        pp.require( XmlEventType.TEXT, null, null);
        Assert.equals("\t", pp.getText());
        pp.next();
        pp.require( XmlEventType.END_TAG, "URI", "s");
        
        pp.next();
        pp.require( XmlEventType.END_TAG, "", "test");
        pp.next();
        pp.require( XmlEventType.END_DOCUMENT, null, null);
        
        //now check that require will NOT skip white space
        pp = factory.newPullParser();
        pp.setInput( new StringInput( "<m:s xmlns:m='URI'>\t</m:s>"));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        pp.next();
        pp.require( XmlEventType.START_TAG, "URI", "s");
        pp.next();
        try
        {
            pp.require( XmlEventType.END_TAG, "URI", "s");
            Assert.fail("require() MUST NOT skip white spaces");
        } catch(ex:XmlPullParserException){}
    
    }
    
    public function testXmlDecl() : Void
    {
        var pp : XmlPullParser = factory.newPullParser();
        
        pp.setInput( new StringInput( "<?xml version='1.0'?><foo/>"));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "foo");
        pp.next();
        pp.require( XmlEventType.END_TAG, null, "foo");
        pp.next();
        pp.require( XmlEventType.END_DOCUMENT, null, null);
        
        pp.setInput( new StringInput(
                        "<?xml  version=\"1.0\" \t encoding='UTF-8' \nstandalone='yes' ?><foo/>"));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "foo");
        
        pp.setInput( new StringInput(
                        "<?xml  version=\"1.0\" \t encoding='UTF-8' \nstandalone='yes' ?><foo/>"));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        Assert.isNull(pp.getProperty(PROPERTY_XMLDECL_VERSION));
        Assert.isNull(pp.getProperty(PROPERTY_XMLDECL_STANDALONE));
        Assert.isNull(pp.getProperty(PROPERTY_XMLDECL_CONTENT));
        
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "foo");
        var xmlDeclVersion : String = Std.instance(pp.getProperty(PROPERTY_XMLDECL_VERSION), String);
        if(xmlDeclVersion != null)
        {
            Assert.equals("1.0", xmlDeclVersion);
        }
        var xmlDeclStandalone  = pp.getProperty(PROPERTY_XMLDECL_STANDALONE);
        if(xmlDeclStandalone != null)
        {
            Assert.equals(xmlDeclStandalone, true);
        }
        var xmlDeclContent : String = Std.instance(pp.getProperty(PROPERTY_XMLDECL_CONTENT), String);
        if(xmlDeclContent != null)
        {
            var expected = "  version=\"1.0\" \t encoding='UTF-8' \nstandalone='yes' ";
            Assert.equals(Util.printable(expected), Util.printable(xmlDeclContent));
        }

        
        // XML decl without required verion
        pp.setInput( new StringInput( "<?xml test?><foo/>"));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        
        // check that proerties are reset
        Assert.isNull(pp.getProperty(PROPERTY_XMLDECL_VERSION));
        Assert.isNull(pp.getProperty(PROPERTY_XMLDECL_STANDALONE));
        Assert.isNull(pp.getProperty(PROPERTY_XMLDECL_CONTENT));
        
        try
        {
            pp.next();
            Assert.fail("expected exception for invalid XML declaration without version");
        } catch(ex:XmlPullParserException){}

        
        pp.setInput( new StringInput(
                        "<?xml version=\"1.0\" standalone='yes' encoding=\"UTF-8\" ?>\n<foo/>"));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        try
        {
            pp.next();
            Assert.fail("expected exception for invalid XML declaration with standalone at wrong position");
        } catch(ex:XmlPullParserException){}

        
        pp.setInput( new StringInput(
                        "<?xml  version=\"1.0\" \t encoding='UTF-8' \nstandalone='yes' ?>"
                            +"<!--comment--><foo/>"));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        // make sure that XMLDecl is not reported
        pp.nextToken();
        pp.require( XmlEventType.COMMENT, null, null);
        pp.nextToken();
        pp.require( XmlEventType.START_TAG, null, "foo");
        pp.next();
        pp.require( XmlEventType.END_TAG, null, "foo");
        pp.next();
        pp.require( XmlEventType.END_DOCUMENT, null, null);
    
    }
    
    public function testComments() : Void
    {
        var pp : XmlPullParser = factory.newPullParser();
        pp.setInput( new StringInput( "<foo><!-- B+, B or B---></foo>"));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "foo");
        try
        {
            pp.next();
            Assert.fail("expected eception for invalid comment ending with --->");
        } catch(ex:XmlPullParserException){}
        checkContentMerging(pp, "<foo><!-- this is a comment --></foo>", null);
        checkContentMerging(pp, "<foo>\n<!-- this is a comment -->\n</foo>", "\n\n");
        checkContentMerging(pp, "<foo><!-- this is a comment -->\n</foo>", "\n");
        checkContentMerging(pp, "<foo>al<!-- this is a comment -->ek\n</foo>", "alek\n");
        checkContentMerging(pp, "<foo>a<!-- sdds \n>-->l\n<!-- this is a comment -->ek\n</foo>", "al\nek\n");
    }
    
    public function checkContentMerging(pp:XmlPullParser, xml:String, expected:String) : Void
        
        {
        pp.setInput( new StringInput( xml));
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "foo");
        if(expected != null)
        {
            pp.next();
            pp.require( XmlEventType.TEXT, null, null);
            Assert.equals(Util.printable(expected), Util.printable(pp.getText()));
            Assert.equals(expected, pp.getText());
        }
        pp.next();
        pp.require( XmlEventType.END_TAG, null, "foo");
    }
    
    public function testComments2() : Void
    {
         checkComments2a2(" <b>\r\n</b>\n ", " <b>\n</b>\n ");
         checkComments2a2(" <b>\r\n</b>\r\n ", " <b>\n</b>\n ");
         checkComments2a2(" <b>\r\n</b>\r\n", " <b>\n</b>\n");
         checkComments2a2(" <b>\r\n</b>\r\n\r\n", " <b>\n</b>\n\n");
         checkComments2a2("\r\n<b>\n</b>\n ", "\n<b>\n</b>\n ");
         checkComments2a2("<b>\r</b>\n ", "<b>\n</b>\n ");
         checkComments2a2("<b>\n</b>\r", "<b>\n</b>\n");
         checkComments2a2("<b>\r\n</b>","<b>\n</b>");
         checkComments2a2("<b>\r\n ? </b>","<b>\n ? </b>");
         
         checkComments2a1("\n <b>\n</b>\n ");
         checkComments2a1("\n<b>\n</b>\n ");
         checkComments2a1(" <b>\n</b>\n ");
         checkComments2a1("<b>\n</b>\n ");
         checkComments2a1("<b>\n</b>\n");
         checkComments2a1("<b>\n</b>");
         checkComments2a1("<b>- -</b>");
         checkComments2a1("<b>/b>");
         checkComments2a1("<b>?? ?/b>");
         checkComments2a1("<?po <b>?? ?/b ?>");
    }
    
    public function checkComments2a1(value:String) : Void
    {
        checkComments2a2(value, value);
    }
    
    public function checkComments2a2(value:String, expected:String) : Void
    {
        var pp : XmlPullParser = factory.newPullParser();
        var XML = "<foo>\n<!--"+value+"-->\n</foo>";
        checkContentMerging(pp, XML, "\n\n");
        pp.setInput( new StringInput( XML));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "foo");
        pp.nextToken();
        pp.require( XmlEventType.TEXT, null, null);
        Assert.equals("\n", pp.getText());
        pp.nextToken();
        pp.require( XmlEventType.COMMENT, null, null);
        //System.err.println(getClass()+" expected="+printable(expected)+" text="+printable(pp.getText()));
        Assert.equals(Util.printable(expected), Util.printable(pp.getText()));
        Assert.equals(expected, pp.getText());
        pp.nextToken();
        pp.require( XmlEventType.TEXT, null, null);
        Assert.equals("\n", pp.getText());
        pp.nextToken();
        pp.require( XmlEventType.END_TAG, null, "foo");
    }

    
    public function testPI() : Void
    {
        var pp : XmlPullParser = factory.newPullParser();
        //System.out.println(getClass()+"-pp="+pp);
        
        pp.setInput( new StringInput( "<foo><?XmLsdsd test?></foo>"));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "foo");
        pp.next();
        pp.require( XmlEventType.END_TAG, null, "foo");
        
        pp.setInput( new StringInput( "<foo><?XmL test?></foo>"));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "foo");
        try
        {
            pp.next();
            Assert.fail("expected exception for invalid PI starting with xml");
        } catch(ex:XmlPullParserException){}
        
        pp.setInput( new StringInput( "<foo><?pi test?></foo>"));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "foo");
        pp.nextToken();
        pp.require( XmlEventType.PROCESSING_INSTRUCTION, null, null);
        Assert.equals("pi test", pp.getText());
        pp.next();
        pp.require( XmlEventType.END_TAG, null, "foo");
        
        checkContentMerging(pp, "<foo><?this is PI ?></foo>", null);
        checkContentMerging(pp, "<foo>\n<?this is PI ?>\n</foo>", "\n\n");
        checkContentMerging(pp, "<foo><?this is PI ?>\n</foo>", "\n");
        try
        {
            checkContentMerging(pp, "<foo><? this is PI ?>\n</foo>", "\n");
            Assert.fail("expected parser to fail as there is space in PI between <? and target");
        } catch(e:xmlpull.XmlPullParserException)
        {
        }
    
    }
    
    public function testPi2() : Void
    {
         checkPi2a2("<b>\r\n</b>\n ", "<b>\n</b>\n ");
         checkPi2a2("<b>\r\n</b>\r\n ", "<b>\n</b>\n ");
         checkPi2a2("<b>\r\n</b>\r\n", "<b>\n</b>\n");
         checkPi2a2("<b>\r\n</b>\r\n\r\n", "<b>\n</b>\n\n");
         checkPi2a2("\r\n<b>\n</b>\n ", "\n<b>\n</b>\n ");
         checkPi2a2("<b>\r</b>\n ", "<b>\n</b>\n ");
         checkPi2a2("<b>\n</b>\r", "<b>\n</b>\n");
         checkPi2a2("<b>\r\n</b>","<b>\n</b>");
         
         checkPi2a1("\n <b>\n</b>\n ");
         checkPi2a1("\n<b>\n</b>\n ");
         checkPi2a1(" <b>\n</b>\n ");
         checkPi2a1("<b>\n</b>\n ");
         checkPi2a1("<b>\n</b>\n");
         checkPi2a1("<b>\n</b>");
         checkPi2a1("<b>- -</b>");
         checkPi2a1("<b>/b>");
         checkPi2a1("<b><!-- -->cx-</b>");
         checkPi2a1("<b><!-- --? ?- ?? ? ></b>");
    }
    
    public function checkPi2a1(value:String) : Void
    {
        checkPi2a3("pi", value, value);
    }
    
    public function checkPi2a2(value:String, expected:String) : Void
    {
        checkPi2a3("pi", value, expected);
    }
    
    public function checkPi2a3(target:String, value:String, expected:String) : Void
    {
        var pp : XmlPullParser = factory.newPullParser();
        if(target != null)
        {
            value = "pi "+value;
            expected = "pi "+expected;
        }
        var XML = "<foo>\n<?"+value+"?>\n</foo>";
        checkContentMerging(pp, XML, "\n\n");
        pp.setInput( new StringInput( XML));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "foo");
        pp.nextToken();
        pp.require( XmlEventType.TEXT, null, null);
        Assert.equals("\n", pp.getText());
        pp.nextToken();
        pp.require( XmlEventType.PROCESSING_INSTRUCTION, null, null);
        //System.err.println(getClass()+" expected="+printable(expected)+" text="+printable(pp.getText()));
        Assert.equals(Util.printable(expected), Util.printable(pp.getText()));
        Assert.equals(expected, pp.getText());
        pp.nextToken();
        pp.require( XmlEventType.TEXT, null, null);
        Assert.equals("\n", pp.getText());
        pp.nextToken();
        pp.require( XmlEventType.END_TAG, null, "foo");
    }
    
    public function testReportNamespaceAttributes() : Void
    {
        var pp : XmlPullParser = factory.newPullParser();
        Assert.equals(true, pp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        
        try
        {
            pp.setFeature(XmlConstants.FEATURE_REPORT_NAMESPACE_ATTRIBUTES, true);
        }catch(ex:XmlPullParserException)
        {
            // skip rest of test if parser does not support reporting
            return;
        }
        // see XML Namespaces spec for namespace URIs for 'xml' and 'xmlns'
        //   xml is bound to http://www.w3.org/XML/1998/namespace
        //   "(...) The prefix xmlns is used only for namespace bindings
        //     and is not itself bound to any namespace name. (...)
        // however it is typically bound to "http://www.w3.org/2000/xmlns/"
        //   in some contexts such as DOM
        // http://www.w3.org/TR/REC-xml-names/#ns-using
        var XML_MISC_ATTR = "<test xmlns='Some-Namespace-URI' xmlns:n='Some-Other-URI'"+
            " a='a' b='b' xmlns:m='Another-URI' m:a='c' n:b='d' n:x='e' xml:lang='en'"+
            "/>\n"+
            "";
        pp.setInput(new StringInput(XML_MISC_ATTR));
        pp.next();
        //pp.readStartTag(stag);
        Assert.equals("test", pp.getName());
        Assert.equals("Some-Namespace-URI", pp.getNamespace());
        
        Assert.equals("a", pp.getAttributeValue("","a"));
        Assert.equals("b", pp.getAttributeValue("","b"));
        Assert.equals(null, pp.getAttributeValue("", "m:a"));
        Assert.equals(null, pp.getAttributeValue("", "n:b"));
        Assert.equals(null, pp.getAttributeValue("", "n:x"));
        
        Assert.equals("c", pp.getAttributeValue("Another-URI", "a"));
        Assert.equals("d", pp.getAttributeValue("Some-Other-URI", "b"));
        Assert.equals("e", pp.getAttributeValue("Some-Other-URI", "x"));
        Assert.equals("en", pp.getAttributeValue("http://www.w3.org/XML/1998/namespace", "lang"));

        
        Util.checkAttribNs(pp, 0, null, "", "xmlns", "Some-Namespace-URI");
        Util.checkAttribNs(pp, 1, "xmlns", "http://www.w3.org/2000/xmlns/","n","Some-Other-URI");
        Util.checkAttribNs(pp, 2, null, "", "a", "a");
        Util.checkAttribNs(pp, 3, null, "", "b", "b");
        Util.checkAttribNs(pp, 4, "xmlns", "http://www.w3.org/2000/xmlns/","m","Another-URI");
        Util.checkAttribNs(pp, 5, "m", "Another-URI","a","c");
        Util.checkAttribNs(pp, 6, "n", "Some-Other-URI","b","d");
        Util.checkAttribNs(pp, 7, "n", "Some-Other-URI","x","e");
        Util.checkAttribNs(pp, 8, "xml", "http://www.w3.org/XML/1998/namespace", "lang", "en");
    }

    
    public function testRoundtripNext() : Void
    {
        // check that entiy reference is reported in start tag as unexpanded when roundtrip is enabled
        var SAMPLE_XML = "<foo attEol='a\r\nv\n\ra\n\r\r\nio\rn\nica' attr=\"baz &amp; bar\"/>";
        
        var pp : XmlPullParser = factory.newPullParser();
        Assert.equals(true, pp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        pp.setInput(new StringInput(SAMPLE_XML));
        
        try
        {
            pp.setFeature(XmlConstants.FEATURE_XML_ROUNDTRIP, true);
        } catch (ex:Dynamic)
        {
        }
        // did we succeeded?
        var roundtripSupported : Bool = pp.getFeature(XmlConstants.FEATURE_XML_ROUNDTRIP);

        
        pp.next();
        var attrValue : String = pp.getAttributeValue(XmlConstants.NO_NAMESPACE, "attr");
        Assert.equals("baz & bar", attrValue);
        var attEolValue : String = pp.getAttributeValue(XmlConstants.NO_NAMESPACE, "attEol");
        Assert.equals("a v  a   io n ica", attEolValue);
        if(roundtripSupported)
        {
            var text : String = pp.getText();
            Assert.equals(SAMPLE_XML, text);
        }
    
    }

    
    public function testEntities() : Void
    {
        var pp : XmlPullParser = factory.newPullParser();
        var XML = "<root><foo>&amp;</foo>\n<foo>&lt;&amp;</foo></root>";
        pp.setInput( new StringInput( XML));
        pp.require( XmlEventType.START_DOCUMENT, null, null);
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "root");
        
        pp.next();
        pp.require( XmlEventType.START_TAG, null, "foo");
        pp.next();
        pp.require( XmlEventType.TEXT, null, null);
        Assert.equals("&", pp.getText());
        pp.nextTag();
        pp.require( XmlEventType.END_TAG, null, "foo");
        
        pp.nextTag();
        pp.require( XmlEventType.START_TAG, null, "foo");
        pp.next();
        pp.require( XmlEventType.TEXT, null, null);
        Assert.equals("<&", pp.getText());
        pp.nextToken();
        pp.require( XmlEventType.END_TAG, null, "foo");
        
        pp.nextToken();
        pp.require( XmlEventType.END_TAG, null, "root");
    }
    
}

