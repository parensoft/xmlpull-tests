/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

/* -*-             c-basic-offset: 4; indent-tabs-mode: nil; -*-  //------100-columns-wide------>|*/
// for license see accompanying LICENSE_TESTS.txt file (available also at http://www.xmlpull.org)

package xmlpull.tests;

import utest.Assert;



import haxe.io.BytesOutput;

import haxe.io.BytesInput;
import haxe.io.StringInput;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;
import xmlpull.XmlSerializer;

/**
 * Simple test to verify serializer (with no namespaces)
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestSerialize 
{
    private var factory : XmlPullParserFactory;
    private var xpp : XmlPullParser;

    
    
    public function new()
    {
        
    }
    
    private function setup() : Void
    {
        factory = Util.newFactory();
        xpp = factory.newPullParser();
        Assert.equals(false, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
    }
    
    private function teardown() : Void
    {
    }
    
    public function testSimpleWriter() : Void
    {
        var ser : XmlSerializer = factory.newSerializer();
        xpp = factory.newPullParser();
        
        //assert there is error if trying to write
        
        //assert there is error if trying to write
        try
        {
            ser.startTag(null, "foo");
            Assert.fail("exception was expected of serializer if no input was set on parser");
        } catch(ex:Dynamic) {}
        
        ser.setOutput(null);
        
        //assert there is error if trying to write
        try
        {
            ser.startTag(null, "foo");
            Assert.fail("exception was expected of serializer if no input was set on parser");
        } catch(ex:Dynamic) {}
        
        var sw = new BytesOutput();
        
        ser.setOutput(sw);
        
        //assertEquals(null, ser.getOutputEncoding());
        
        ser.startDocument("ISO-8859-1", true);
        ser.startTag(null, "foo");
        
        ser.endTag(null, "foo");
        ser.endDocument();
        
        // now validate that can be deserialzied
        
        //xpp.setInput(new StringReader("<foo></foo>"));
        var serialized : String = sw.getBytes().toString();

        xpp.setInput(new StringInput(serialized));
        
        Assert.equals(null, xpp.getInputEncoding());
        Util.checkParserState(xpp, 0, XmlEventType.START_DOCUMENT, null, null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.START_TAG, "foo", null, xpp.isEmptyElementTag() /*empty*/, 0);
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.END_TAG, "foo", null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 0, XmlEventType.END_DOCUMENT, null, null, false, -1);
    }
    
    public function testSimpleStream() : Void
    {
        var ser : XmlSerializer = factory.newSerializer();
        
        var baos = new BytesOutput();
        ser.setOutput(baos, "UTF-8");
        ser.startDocument("UTF-8", null);
        ser.startTag(null, "foo");
        ser.text("test");
        ser.endTag(null, "foo");
        ser.endDocument();
        
        //check taking input form input stream
        //byte[] binput = "<foo>test</foo>".getBytes("UTF8");
        
        var binput = baos.getBytes();
        
        xpp.setInputWithEncoding(new BytesInput( binput), "UTF-8");
        Assert.equals("UTF-8", xpp.getInputEncoding());
        
        //xpp.setInput(new StringReader( "<foo/>" ) );
        Util.checkParserState(xpp, 0, XmlEventType.START_DOCUMENT, null, null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.START_TAG, "foo", null, false /*empty*/, 0);
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.TEXT, null, "test", false, -1);
        Assert.equals(false, xpp.isWhitespace());
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.END_TAG, "foo", null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 0, XmlEventType.END_DOCUMENT, null, null, false, -1);
    }
    
    public function testSimpleSerWithAttribute() : Void
    {
        var ser : XmlSerializer = factory.newSerializer();
        // one step further - it has an attribute and content ...
        
        var sw = new BytesOutput();
        
        Assert.equals(0, ser.getDepth());
        ser.setOutput(sw);
        Assert.equals(0, ser.getDepth());
        
        ser.startDocument(null, null);
        Assert.equals(0, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals(null, ser.getName());
        
        ser.startTag(null, "foo");
        Assert.equals(1, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals("foo", ser.getName());
        
        ser.attribute(null, "attrName", "attrVal");
        Assert.equals(1, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals("foo", ser.getName());
        
        ser.text("bar");
        Assert.equals(1, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals("foo", ser.getName());
        
        ser.startTag(null, "p:t");
        Assert.equals(2, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals("p:t", ser.getName());
        
        ser.text("\n\t ");
        
        ser.endTag(null, "p:t");
        Assert.equals(1, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals("foo", ser.getName());
        
        ser.endTag(null, "foo");
        Assert.equals(0, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals(null, ser.getName());
        
        ser.endDocument();
        Assert.equals(0, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals(null, ser.getName());
        
        //xpp.setInput(new StringReader("<foo attrName='attrVal'>bar<p:t>\r\n\t </p:t></foo>"));
        var serialized : String = sw.getBytes().toString();
        xpp.setInput(new StringInput(serialized));
        Util.checkParserState(xpp, 0, XmlEventType.START_DOCUMENT, null, null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.START_TAG, "foo", null, false, 1);
        Util.checkAttrib(xpp, 0, "attrName", "attrVal");
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.TEXT, null, "bar", false, -1);
        Assert.equals(false, xpp.isWhitespace());
        xpp.next();
        Util.checkParserState(xpp, 2, XmlEventType.START_TAG, "p:t", null, false, 0);
        xpp.next();
        Util.checkParserState(xpp, 2, XmlEventType.TEXT, null, "\n\t ", false, -1);
        Assert.isTrue(xpp.isWhitespace());
        xpp.next();
        Util.checkParserState(xpp, 2, XmlEventType.END_TAG, "p:t", null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.END_TAG, "foo", null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 0, XmlEventType.END_DOCUMENT, null, null, false, -1);

    
    }
    
    public function testEscaping() : Void
    {
        var ser : XmlSerializer = factory.newSerializer();
        
        var baos : BytesOutput = new BytesOutput();
        ser.setOutput(baos, "UTF-8");
        ser.startDocument("UTF-8", null);
        ser.startTag("", "foo");
        var s = "test\x08\t\r\n";
        try
        {
            ser.attribute(null, "att", s);
            Assert.fail("expected to fail for control character (<32)");
        } catch(e:XmlPullParserException) {}
        
        ser = factory.newSerializer();
        baos = new BytesOutput();
        ser.setOutput(baos, "UTF-8");
        ser.startDocument("UTF-8", null);
        ser.startTag("", "foo");
        try
        {
            ser.text(s);
        } catch(e:XmlPullParserException) {}
        
        ser = factory.newSerializer();
        baos = new BytesOutput();
        ser.setOutput(baos, "UTF-8");
        ser.startDocument("UTF-8", null);
        ser.startTag(null, "foo");
        s = "test\t\t\r\n";
        var expectedS = "test\x09\t\n";
        ser.attribute(null, "att", s);
        ser.text(s);
        
        ser.endTag(null, "foo");
        ser.endDocument();
        
        //check taking input form input stream
        //byte[] binput = "<foo>test</foo>".getBytes("UTF8");
        
        var binput = baos.getBytes();
        //System.out.println(getClass()+" binput="+printable(new String(binput)));
        
        xpp.setInputWithEncoding(new BytesInput( binput), "UTF-8");
        Assert.equals("UTF-8", xpp.getInputEncoding());
        
        //xpp.setInput(new StringReader( "<foo/>" ) );
        Util.checkParserState(xpp, 0, XmlEventType.START_DOCUMENT, null, null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.START_TAG, "foo", null, false /*empty*/, 1);
        Assert.equals(Util.printable(s), Util.printable(xpp.getAttributeValue(null, "att")));
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.TEXT, null, expectedS, false, -1);
        Assert.equals(false, xpp.isWhitespace());
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.END_TAG, "foo", null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 0, XmlEventType.END_DOCUMENT, null, null, false, -1);
        
        baos = new BytesOutput();
        ser.setOutput(baos, "UTF-8");
        ser.startDocument("UTF-8", null);
        ser.startTag("", "foo");
        s = "test\x00\t\r\n";
        try
        {
            ser.attribute(null, "att", s);
            Assert.fail("expected to fail as zero chartacter is illegal both in XML 1.0 and 1.1");
        } catch(e:Dynamic) {}
        try
        {
            ser.text(s);
            Assert.fail("expected to fail as zero chartacter is illegal both in XML 1.0 and 1.1");
        } catch(e:Dynamic) {}
    }
}

