/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

package xmlpull.tests;

import utest.Assert;

import haxe.io.StringInput;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;
import xmlpull.XmlConstants;

/**
 * Test end-of-line normalization
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestEolNormalization {
     var factory: XmlPullParserFactory;


    public function new() {}

    public function setup() {
        factory = Util.newFactory();

        factory.setNamespaceAware(true);
        Assert.equals(true, factory.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        Assert.equals(false, factory.getFeature(XmlConstants.FEATURE_VALIDATION));
    }

    public function testNormalizeLine() {

        var pp = factory.newPullParser();

        //-----------------------
        // ---- simple tests for end of line normalization

        var simpleR = "-\n-\r-\r\n-\n\r-";

        // element content EOL normalizaton

        var tagSimpleR = "<test>"+simpleR+"</test>";

        var expectedSimpleN = "-\n-\n-\n-\n\n-";

        parseOneElement(pp, tagSimpleR, true);
        Assert.equals(XmlEventType.TEXT, pp.next());
        Assert.equals(Util.printable(expectedSimpleN), Util.printable(pp.getText()));

        // attribute content normalization

        var attrSimpleR = "<test a=\""+simpleR+"\"/>";

        var normalizedSimpleN = "- - - -  -";

        parseOneElement(pp, attrSimpleR, true);
        var attrVal = pp.getAttributeValue("","a");

        //TODO Xerces2
        Assert.equals(Util.printable(normalizedSimpleN), Util.printable(attrVal));

        //-----------------------
        // --- more complex example with more line engins together

        var firstR =
            "\r \r\n \n\r \n\n \r\n\r \r\r \r\n\n \n\r\r\n\r"+
            "";

        // element content

        var tagR =
            "<m:test xmlns:m='Some-Namespace-URI'>"+
            firstR+
            "</m:test>\r\n";

        var expectedN =
            "\n \n \n\n \n\n \n\n \n\n \n\n \n\n\n\n";

        parseOneElement(pp, tagR, true);
        Assert.equals(XmlEventType.TEXT, pp.next());
        Assert.equals(Util.printable(expectedN), Util.printable(pp.getText()));

        // attribute value

        var attrR =
            "<m:test xmlns:m='Some-Namespace-URI' fifi='"+firstR+"'/>";

        var normalizedN =
            "                       ";

        parseOneElement(pp, attrR, true);
        attrVal = pp.getAttributeValue("","fifi");
        //System.err.println("attrNormalized.len="+normalizedN.length());
        //System.err.println("attrVal.len="+attrVal.length());

        //TODO Xerces2
        Assert.equals(Util.printable(normalizedN), Util.printable(attrVal));


        //-----------------------
        // --- even more complex

        var manyLineBreaks =
            "fifi\r&amp;\r&amp;\r\n foo &amp;\r bar \n\r\n&quot;"+
            firstR;

        var manyTag =
            "<m:test xmlns:m='Some-Namespace-URI'>"+
            manyLineBreaks+
            "</m:test>\r\n";

        var manyExpected =
            "fifi\n&\n&\n foo &\n bar \n\n\""+
            expectedN;
        //"\r \r\n \n\r \n\n \r\n\r \r\r \r\n\n \n\r\r\n\r";

        parseOneElement(pp, manyTag, true);
        Assert.equals(XmlEventType.TEXT, pp.next());
        Assert.equals(Util.printable(manyExpected), Util.printable(pp.getText()));
        Assert.equals(manyExpected, pp.getText());

        Assert.equals(pp.next(), XmlEventType.END_TAG);
        Assert.equals("test", pp.getName());

        // having \r\n as last characters is the hardest case
        //Assert.equals(XmlEventType.CONTENT, pp.next());
        //Assert.equals("\n", pp.readContent());
        Assert.equals(pp.next(), XmlEventType.END_DOCUMENT);


        var manyAttr =
            "<m:test xmlns:m='Some-Namespace-URI' fifi='"+manyLineBreaks+"'/>";

        var manyNormalized =
            "fifi & &  foo &  bar   \""+
            normalizedN;

        parseOneElement(pp, manyAttr, true);
        attrVal = pp.getAttributeValue("","fifi");
        //TODO Xerces2
        Assert.equals(Util.printable(manyNormalized), Util.printable(attrVal));

    }

    private function parseOneElement(
        pp: XmlPullParser,
        buf: String,
        supportNamespaces: Bool) {
        //pp.setInput(buf.toCharArray());
        pp.setInput(new StringInput(buf));
        //pp.setNamespaceAware(supportNamespaces);
        pp.setFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES, supportNamespaces);
        //pp.setAllowedMixedContent(false);
        pp.next();
        //pp.readStartTag(stag);
        if(supportNamespaces) {
            Assert.equals("test", pp.getName());
        } else {
            Assert.equals("m:test", pp.getName());
        }
    }


}

