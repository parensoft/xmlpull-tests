/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

package xmlpull.tests;

import haxe.io.StringInput;
import haxe.io.BytesInput;
import haxe.io.Bytes;

import utest.Assert;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;
import xmlpull.XmlEventType;

/**
 * Test if entity replacement works ok.
 * This test is designe to work bboth for validating and non validating parsers!
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestEntityReplacement {
    private var factory: XmlPullParserFactory;

    public function new() {}

    public function setup() {
        factory = Util.newFactory();

        factory.setNamespaceAware(true);
        Assert.equals(true, factory.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        //Assert.equals(false, factory.getFeature(XmlConstants.FEATURE_VALIDATION));
    }

    public function testEntityReplacement() {
        // taken from http://www.w3.org/TR/REC-xml#sec-entexpand
        var XML_ENTITY_EXPANSION =
            "<?xml version='1.0'?>\n"+
            "<!DOCTYPE test [\n"+
            "<!ELEMENT test (#PCDATA) >\n"+
            "<!ENTITY % xx '&#37;zz;'>\n"+
            "<!ENTITY % zz '&#60;!ENTITY tricky \"error-prone\" >' >\n"+
            "%xx;\n"+
            "]>"+
            "<test>This sample shows a &tricky; method.</test>";

        var pp = factory.newPullParser();
        // default parser must work!!!!
        pp.setInput(new StringInput( XML_ENTITY_EXPANSION ) );
        if(pp.getFeature( XmlConstants.FEATURE_PROCESS_DOCDECL ) == false) {
            pp.defineEntityReplacementText("tricky", "error-prone");
        }
        checkEntityReplacementInt(pp);

        // now we try for no FEATURE_PROCESS_DOCDECL
        pp.setInput(new StringInput( XML_ENTITY_EXPANSION ) );
        try {
            pp.setFeature( XmlConstants.FEATURE_PROCESS_DOCDECL, false );
        } catch( ex: Dynamic ){
        }
        if( pp.getFeature( XmlConstants.FEATURE_PROCESS_DOCDECL ) == false ) {
            pp.defineEntityReplacementText("tricky", "error-prone");
            checkEntityReplacementInt(pp);
        }

        // try to use FEATURE_PROCESS_DOCDECL if supported
        pp.setInput(new StringInput( XML_ENTITY_EXPANSION ) );
        try {
            pp.setFeature( XmlConstants.FEATURE_PROCESS_DOCDECL, true );
        } catch( ex: Dynamic ){
        }
        if( pp.getFeature( XmlConstants.FEATURE_PROCESS_DOCDECL ) ) {
            checkEntityReplacementInt(pp);
        }

        // try to use FEATURE_PROCESS_DOCDECL if supported
        pp.setInput(new StringInput( XML_ENTITY_EXPANSION ) );
        try {
            pp.setFeature( XmlConstants.FEATURE_VALIDATION, true );
        } catch( ex: Dynamic ){
        }
        if( pp.getFeature( XmlConstants.FEATURE_VALIDATION ) ) {
            checkEntityReplacementInt(pp);
        }

    }

    private function checkEntityReplacementInt(pp: XmlPullParser) {
        pp.next();
        Util.checkParserStateNs(pp, 1, XmlEventType.START_TAG,
                           null, 0, "", "test", null, false, 0);
        pp.next();
        Util.checkParserStateNs(pp, 1, XmlEventType.TEXT, null, 0, null, null,
                           "This sample shows a error-prone method.", false, -1);
        pp.next();
        Util.checkParserStateNs(pp, 1, XmlEventType.END_TAG,
                           null, 0, "", "test", null, false, -1);
        pp.nextToken();
        Util.checkParserStateNs(pp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);

    }

    /**
     * Additional tests to confirm that
     * <a href="http://www.extreme.indiana.edu/bugzilla/show_bug.cgi?id=192">Bug 192
     * Escaped characters disappear in certain cases</a>
     * is not present in XmlPull implementations.
     */
    public function checkLastEntityInt(xml: String, expectedText: String): Void {
        var stream = new BytesInput(Bytes.ofString(xml));
        var pp = factory.newPullParser();
        // default parser must work!!!!
        pp.setInputWithEncoding(stream, "UTF-8" );
        pp.nextTag();
        Assert.equals(XmlEventType.START_TAG, pp.getEventType());
        var c = pp.nextText();
        Assert.equals(expectedText, c);
        Assert.equals(XmlEventType.END_TAG, pp.getEventType());
    }

    public function testLastEntity(): Void {
        checkLastEntityInt(
            "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><document>&lt;text&gt;</document>",
            "<text>"
        );
        checkLastEntityInt(
            "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><document>&lt;text&gt; </document>",
            "<text> "
        );
    }

    /*
    public function testDefineEntityBeforeSetInput(): Void {
        var pp = factory.newPullParser();
        pp.defineEntityReplacementText("foo", "bar");
        pp.setInput(new StringInput( "<foo/>" ) );

    }
    */


}

