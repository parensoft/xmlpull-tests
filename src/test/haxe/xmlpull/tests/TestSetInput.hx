/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

/* -*-             c-basic-offset: 4; indent-tabs-mode: nil; -*-  //------100-columns-wide------>|*/
// for license see accompanying LICENSE_TESTS.txt file (available also at http://www.xmlpull.org)

package xmlpull.tests;

import utest.Assert;



import haxe.io.Bytes;
import haxe.io.BytesInput;
import haxe.io.Input;
import haxe.io.StringInput;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;

/**
 * Tests to determine that setInput works as expected for different encodings.
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestSetInput 
{
    private var factory : XmlPullParserFactory;
    
    public function new()
    {
        
    }
    
    private function setup() : Void
    {
        factory = Util.newFactory();
        factory.setFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES, true);
        Assert.equals(true, factory.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
    }
    
    private function teardown() : Void
    {
    }
    
//!    public function testSetReader() : Void
//!    {
//!        var xpp : XmlPullParser = factory.newPullParser();
//!        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
//!        
//!        xpp.setInput(null);
//!        Assert.equals(XmlEventType.START_DOCUMENT, xpp.getEventType());
//!        try
//!        {
//!            xpp.next();
//!            Assert.fail("exception was expected of next() if no input was set on parser");
//!        } catch(ex:XmlPullParserException) {}
//!
//!        
//!        // make input suspectible to read ...
//!        var reader : ReaderWrapper = new ReaderWrapper(
//!            new StringInput("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><foo/>"));
//!        Assert.equals("no read() called in just contructed reader", false, reader.calledRead());
//!        
//!        xpp.setInput(reader);
//!        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
//!        Assert.equals("read() not called before next()", false, reader.calledRead());
//!        
//!        xpp.next();
//!        Assert.equals("read() must be called after next()", true, reader.calledRead());
//!        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "foo", null, true/*empty*/, 0);
//!    }
    
    public function testSetInput() : Void
    {
        var xpp : XmlPullParser = factory.newPullParser();
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        
        // another test
        
        var binput = Bytes.ofString("<?xml version=\"1.0\" encoding=\"UTF-8\"?><foo/>");
        var isw : InputStreamWrapper = new InputStreamWrapper(
            new BytesInput( binput));
        Assert.equals(false, isw.calledRead, "no read() called in just contructed reader");
        
        xpp.setInputWithEncoding(isw, "UTF-8");
        Assert.equals("UTF-8", xpp.getInputEncoding());
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        Assert.equals(false, isw.calledRead, "read() not called before next()");
        
        xpp.nextToken();
        Assert.equals(true, isw.calledRead, "read() must be called after next()");
        
        //needs to resolve:
        // java.lang.InternalError: Converter malfunction (UTF-16) -- please submit a bug report via http://java.sun.com/cgi-bin/bugreport.cgi
        
        //
        //      // add BOM
        //        //byte[] binput1 = new byte[]{((byte)'\u00FE'), ((byte)'\u00FF')};
        //        //byte[] binput1 = new byte[]{((byte)'\u00FF'), ((byte)'\u00FE')};
        //      byte[] binput1 = new byte[0];
        //        byte[] binput2 =
        //            ("<?xml version=\"1.0\" encoding=\"UTF16\"?><foo/>").getBytes("UTF16");
        //        binput = new byte[ binput1.length + binput2.length ] ;
        //        System.arraycopy(binput1, 0, binput, 0, binput1.length);
        //        System.arraycopy(binput2, 0, binput, binput1.length, binput2.length);
        //        isw = new InputStreamWrapper(
        //            new ByteArrayInputStream( binput ));
        //        assertEquals("no read() called in just contructed reader", false, isw.calledRead());
        //
        //        //xpp.setInput(isw, "UTF-16" ); //TODO why Xerces2 causes java Unicode decoder to fail ????
        //      xpp.setInput(isw, "UTF16" );
        //        //assertEquals("UTF-16", xpp.getInputEncoding());
        //        checkParserStateNs(xpp, 0, XmlPullParser.START_DOCUMENT, null, 0, null, null, null, false, -1);
        //        assertEquals("read() not called before next()", false, isw.calledRead());
        //
        //        xpp.nextToken();
        //        assertEquals("read() must be called after next()", true, isw.calledRead());
        //
        // check input detecting  -- for mutlibyte sequences ...
        var FEATURE_DETECT_ENCODING = "http://xmlpull.org/v1/doc/features.html#detect-encoding";
        if(xpp.getFeature(FEATURE_DETECT_ENCODING))
        {
            
            isw = new InputStreamWrapper(
                new BytesInput( binput));
            Assert.equals(false, isw.calledRead, "no read() called in just contructed reader");
            
            xpp.setInputWithEncoding(isw, null);
            Assert.equals(null, xpp.getInputEncoding());
            
            Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
            //assertEquals("read() not called before next()", false, isw.calledRead());
            
            xpp.nextToken();
            Assert.equals(true, isw.calledRead, "read() must be called after next()");
            Assert.equals("UTF16", xpp.getInputEncoding());
        
        }
        
        // check input detecting  -- default
        binput =
            Bytes.ofString("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><foo/>");
        isw = new InputStreamWrapper(
            new BytesInput( binput));
        Assert.equals(false, isw.calledRead, "no read() called in just contructed reader");
        
        xpp.setInputWithEncoding(isw, null);
        Assert.equals(null, xpp.getInputEncoding());
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        //assertEquals("read() not called before next()", false, isw.calledRead());
        
        xpp.next();
        Assert.equals(true, isw.calledRead, "read() must be called after next()");
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "foo", null, true/*empty*/, 0);
        
        if(xpp.getFeature(FEATURE_DETECT_ENCODING))
        {
            Assert.equals("ISO-8859-1", xpp.getInputEncoding());
        }
    }


    public function testSetInputResetPrefix() {
      var xpp = factory.newPullParser();
      xpp.setFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES, true);

      xpp.setInput(new StringInput("<ns:foo xmlns:ns=\"blabla\" />"));

      while(xpp.next() != XmlEventType.START_TAG) {  }

      xpp.setInput(new StringInput("<foo />"));

      Assert.equals(null, xpp.getPrefix());

    }
    
    // ------ allow to detect if input was read
    
}

private class InputStreamWrapper extends Input
{
  var is : Input;
  public var calledRead(default, null) : Bool = false;
  public function new(inputStream: Input)
  {
    is = inputStream;
  }
  public override function readByte() : Int
  {
    calledRead = true;
    return is.readByte();
  }
}

//!private static class ReaderWrapper extends Reader
//!{
//!  var r : Reader;
//!  var calledRead : Bool;
//!  public function calledRead() : Bool { return calledRead; }
//!  public function new()
//!  {
//!    r = reader;
//!  }
//!  public int read(Array<Int>ch, off:Int, len:Int)
//!  {
//!    calledRead = true;
//!    return r.read(ch, off, len);
//!  }
//!  public function close() : Void
//!  {
//!    r.close();
//!  }
//!}


