/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

/* -*-             c-basic-offset: 4; indent-tabs-mode: nil; -*-  //------100-columns-wide------>|*/
// see LICENSE_TESTS.txt in distribution for copyright and license information

package xmlpull.tests;

import haxe.io.StringInput;
import haxe.io.Bytes;
import haxe.io.BytesInput;

import utest.Assert;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;
import xmlpull.XmlConstants;

/**
 * Simple test ot verify pull parser factory
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestSimple {
    var factory: XmlPullParserFactory;

    public function new() {}

    public function setup() {
        factory = Util.newFactory();

        Assert.equals(false, factory.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        Assert.equals(false, factory.getFeature(XmlConstants.FEATURE_VALIDATION));
    }

    public function testSimple() {
        var xpp: XmlPullParser = factory.newPullParser();
        Assert.equals(false, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));

        /* maybe it will return
        // this SHOULD always be OK
        Assert.equals("START_DOCUMENT", XmlEventType.TYPES[XmlEventType.START_DOCUMENT]);
        Assert.equals("END_DOCUMENT", XmlEventType.TYPES[XmlEventType.END_DOCUMENT]);
        Assert.equals("START_TAG", XmlEventType.TYPES[XmlEventType.START_TAG]);
        Assert.equals("END_TAG", XmlEventType.TYPES[XmlEventType.END_TAG]);
        Assert.equals("TEXT", XmlEventType.TYPES[XmlEventType.TEXT]);
        Assert.equals("CDSECT", XmlEventType.TYPES[XmlEventType.CDSECT]);
        Assert.equals("ENTITY_REF", XmlEventType.TYPES[XmlEventType.ENTITY_REF]);
        Assert.equals("IGNORABLE_WHITESPACE", XmlEventType.TYPES[XmlEventType.IGNORABLE_WHITESPACE]);
        Assert.equals("PROCESSING_INSTRUCTION", XmlEventType.TYPES[XmlEventType.PROCESSING_INSTRUCTION]);
        Assert.equals("COMMENT", XmlEventType.TYPES[XmlEventType.COMMENT]);
        Assert.equals("DOCDECL", XmlEventType.TYPES[XmlEventType.DOCDECL]);
        */

        // check setInput semantics
        Assert.equals(XmlEventType.START_DOCUMENT, xpp.getEventType());
        try {
            xpp.next();
            Assert.fail("exception was expected of next() if no input was set on parser");
        } catch(ex: XmlPullParserException) {}

        xpp.setInput(null);
        Assert.equals(XmlEventType.START_DOCUMENT, xpp.getEventType());
        try {
            xpp.next();
            Assert.fail("exception was expected of next() if no input was set on parser");
        } catch(ex: XmlPullParserException) {}

        try {
            xpp.setInputWithEncoding(null, null);
            Assert.fail("exception was expected of setInputWithEncoding() if input stream is null");
        } catch(ex: Dynamic) {}

        xpp.setInput(null);
        Assert.isTrue(xpp.getLineNumber() == -1 || xpp.getLineNumber() >= 1,
            "line number must be -1 or >= 1 not "+xpp.getLineNumber());
        Assert.isTrue(xpp.getColumnNumber() == -1 || xpp.getColumnNumber() >= 0,
            "column number must be -1 or >= 0 not "+xpp.getColumnNumber());

        // check the simplest possible XML document - just one root element
        // for(int i = 1; i <= 2; ++i) {
        xpp.setInput(new StringInput("<foo></foo>"));
        Assert.equals(1, xpp.getLineNumber());
        Assert.equals(0, xpp.getColumnNumber());
        Util.checkParserState(xpp, 0, XmlEventType.START_DOCUMENT, null, null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.START_TAG, "foo", null, false, 0);
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.END_TAG, "foo", null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 0, XmlEventType.END_DOCUMENT, null, null, false, -1);


        //check taking input form input stream
        var binput = Bytes.ofString("<foo/>");
        xpp.setInputWithEncoding(new BytesInput(binput), "UTF-8" );
        //xpp.setInput(new StringReader( "<foo/>" ) );
        Util.checkParserState(xpp, 0, XmlEventType.START_DOCUMENT, null, null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.START_TAG, "foo", null, true, 0);
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.END_TAG, "foo", null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 0, XmlEventType.END_DOCUMENT, null, null, false, -1);

        // one step further - it has content ...


        xpp.setInput(new StringInput("<foo attrName='attrVal'>bar<p:t>\r\n\t </p:t></foo>"));
        Util.checkParserState(xpp, 0, XmlEventType.START_DOCUMENT, null, null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.START_TAG, "foo", null, false, 1);
        Util.checkAttrib(xpp, 0, "attrName", "attrVal");
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.TEXT, null, "bar", false, -1);
        Assert.equals(false, xpp.isWhitespace());
        xpp.next();
        Util.checkParserState(xpp, 2, XmlEventType.START_TAG, "p:t", null, false, 0);
        xpp.next();
        Util.checkParserState(xpp, 2, XmlEventType.TEXT, null, "\n\t ", false, -1);
        Assert.isTrue(xpp.isWhitespace());
        xpp.next();
        Util.checkParserState(xpp, 2, XmlEventType.END_TAG, "p:t", null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 1, XmlEventType.END_TAG, "foo", null, false, -1);
        xpp.next();
        Util.checkParserState(xpp, 0, XmlEventType.END_DOCUMENT, null, null, false, -1);


    }


}

