/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

/* -*-             c-basic-offset: 4; indent-tabs-mode: nil; -*-  //------100-columns-wide------>|*/
// for license see accompanying LICENSE_TESTS.txt file (available also at http://www.xmlpull.org)

package xmlpull.tests;

import utest.Assert;



import haxe.io.BytesOutput;


import haxe.io.BytesInput;
import haxe.io.StringInput;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;
import xmlpull.XmlSerializer;

import xmlpull.XmlConstants.*;

/**
 * Simple test to verify serializer (with namespaces)
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestSerializeWithNs 
{
    private var factory : XmlPullParserFactory;
    private var xpp : XmlPullParser;
    
    public function new()
    {
        
    }
    
    private function setup() : Void
    {
        factory = Util.newFactory();
        factory.setNamespaceAware(true);
        // now validate that can be deserialzied
        xpp = factory.newPullParser();
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
    }
    
    private function teardown() : Void
    {
    }

    
    private function checkSimpleWriterResult(textContent:String) : Void
    {
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "foo", null, xpp.isEmptyElementTag() /*empty*/, 0);
        if(textContent != null)
        {
            xpp.next();
            Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null, textContent, false, -1);
        }
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "foo", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
    }
    
    public function testSimpleWriter() : Void
    {
        var ser : XmlSerializer = factory.newSerializer();
        
        //assert there is error if trying to write
        
        //assert there is error if trying to write
        try
        {
            ser.startTag("", "foo");
            Assert.fail("exception was expected of serializer if no input was set");
        } catch (ex:Dynamic) {}
        
        ser.setOutput(null);
        
        //assert there is error if trying to write
        try
        {
            ser.startTag("", "foo");
            Assert.fail("exception was expected of serializer if no input was set");
        } catch (ex:Dynamic) {}
        
        var sw : BytesOutput = new BytesOutput();
        
        ser.setOutput(sw);
        
        //assertEquals(null, ser.getOutputEncoding());
        
        ser.startDocument("ISO-8859-1", true);
        ser.startTag("", "foo");
        
        //TODO: check that startTag(null, ...) is allowed
        
        ser.endTag("", "foo");
        ser.endDocument();

        
        //xpp.setInput(new StringReader("<foo></foo>"));
        var serialized : String = sw.getBytes().toString();
        xpp.setInput(new StringInput(serialized));
        
        Assert.equals(null, xpp.getInputEncoding());
        checkSimpleWriterResult(null);
    }

    
    public function testSimpleOutputStream() : Void
    {
        var ser : XmlSerializer = factory.newSerializer();

        
        var baos : BytesOutput = new BytesOutput();
        ser.setOutput(baos, "UTF-8");
        ser.startDocument("UTF-8", null);
        ser.startTag("", "foo");
        var text = "\"test<&>&amp;";
        ser.text(text);
        ser.endTag("", "foo");
        ser.endDocument();
        
        //check taking input form input stream
        //byte[] binput = "<foo>test</foo>".getBytes("UTF8");
        
        var binput = baos.getBytes();
        
        xpp.setInputWithEncoding(new BytesInput( binput), "UTF-8");
        Assert.equals("UTF-8", xpp.getInputEncoding());
        
        //xpp.setInput(new StringReader( "<foo/>" ) );
        
        checkSimpleWriterResult(text);
    
    }
    
    public function testNamespaceGeneration() : Void
    {
        var ser : XmlSerializer = factory.newSerializer();
        //System.out.println(getClass()+" ser="+ser);
        
        var sw : BytesOutput = new BytesOutput();
        ser.setOutput(sw);
        
        //assertEquals(null, ser.getOutputEncoding());
        
        ser.startDocument("ISO-8859-1", true);
        
        ser.setPrefix("boo", "http://example.com/boo");
        ser.startTag("", "foo");
        ser.attribute("http://example.com/boo", "attr", "val");
        
        var booPrefix : String = ser.getPrefix("http://example.com/boo", false);
        Assert.equals("boo", booPrefix);
        
        //check that new prefix may be generated after startTag and used as attribbute value
        var newPrefix : String = ser.getPrefix("http://example.com/bar", true);
        ser.attribute("http://example.com/bar", "attr", "val");
        
        ser.startTag("http://example.com/bar", "foo");
        ser.endTag("http://example.com/bar", "foo");
        
        var checkPrefix : String = ser.getPrefix("http://example.com/bar", false);
        Assert.equals(newPrefix, checkPrefix);
        
        ser.endTag("", "foo");
        checkPrefix = ser.getPrefix("http://example.com/bar", false);
        Assert.equals(null, checkPrefix);
        
        ser.endDocument();

        
        //xpp.setInput(new StringReader("<foo></foo>"));
        var serialized : String = sw.getBytes().toString();
        //System.out.println(getClass()+" serialized="+serialized);
        //xpp.setInput(new StringReader(serialized));
    }
    
    public function xxtestMisc() : Void // docdecls unsupported by parser
    {
        var ser : XmlSerializer = factory.newSerializer();
        var sw : BytesOutput = new BytesOutput();
        ser.setOutput(sw);
        
        Assert.equals(0, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals(null, ser.getName());
        
        // all comments etc
        ser.startDocument(null, true);
        Assert.equals(0, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals(null, ser.getName());

        
        var docdecl = " foo [\n"+
            "<!ELEMENT foo (#PCDATA|bar)* >\n"+
            "<!ELEMENT pbar (#PCDATA) >\n"
            +"]";
        ser.docdecl(docdecl);
        ser.processingInstruction("pi test");
        var iws = "\n\t";
        ser.ignorableWhitespace(iws);
        
        Assert.equals(0, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals(null, ser.getName());
        
        ser.startTag(null, "foo");
        Assert.equals(1, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals("foo", ser.getName());

        
        //check escaping & < > " '
        var attrVal = "attrVal&<>\"''&amp;";
        //final String attrVal = "attrVal&;";
        ser.attribute(null, "attrName", attrVal);
        
        Assert.equals(1, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals("foo", ser.getName());
        
        ser.entityRef("amp");
        var cdsect = "hello<test>\"test";
        ser.cdsect(cdsect);
        
        ser.setPrefix("ns1", "usri2");
        
        Assert.equals(1, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals("foo", ser.getName());
        
        ser.startTag("uri1", "bar");
        Assert.equals(2, ser.getDepth());
        Assert.equals("uri1", ser.getNamespace());
        Assert.equals("bar", ser.getName());
        
        var text = "test\n\ntest";
        ser.text(text);
        
        var comment = "comment B- ";
        ser.comment(comment);
        Assert.equals(2, ser.getDepth());
        Assert.equals("uri1", ser.getNamespace());
        Assert.equals("bar", ser.getName());
        
        Assert.equals(2, ser.getDepth());
        Assert.equals("uri1", ser.getNamespace());
        Assert.equals("bar", ser.getName());

        
        ser.endDocument(); // should close unclosed foo and bar start tag
        Assert.equals(0, ser.getDepth());
        Assert.equals(null, ser.getNamespace());
        Assert.equals(null, ser.getName());
        
        // -- now check that we get back what we serialized ...
        
        var serialized : String = sw.getBytes().toString();
        //System.out.println(getClass()+" serialized="+serialized);
        xpp.setInput(new StringInput(serialized));
        xpp.setFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES, true);
        
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        
        xpp.nextToken();
        Util.checkParserStateNs9(xpp, 0, XmlEventType.DOCDECL, null, 0, null, null, false, -1);
        var gotDocdecl : String = xpp.getText();
        if(gotDocdecl != null)
        {
            Assert.equals(Util.printable(docdecl), Util.printable(gotDocdecl));
        }
        
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 0, XmlEventType.PROCESSING_INSTRUCTION, null, 0, null, null, "pi test", false, -1);

        
        xpp.nextToken();
        if(xpp.getEventType() == XmlEventType.IGNORABLE_WHITESPACE)
        {
            var expectedIws : String = Util.gatherTokenText(xpp, XmlEventType.IGNORABLE_WHITESPACE, true);
            Assert.equals(Util.printable(iws), Util.printable(expectedIws));
        }
        
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "foo", null, false, 1);
        Util.Util.checkAttribNs(xpp, 0, null, "", "attrName", attrVal);
        
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 1, XmlEventType.ENTITY_REF, null, 0, null, "amp", "&", false, -1);
        
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 1, XmlEventType.CDSECT, null, 0, null, null, cdsect, false, -1);
        Assert.equals(false, xpp.isWhitespace());
        
        xpp.nextToken();
        Util.checkParserStateNs8(xpp, 2, XmlEventType.START_TAG, 2, "uri1", "bar", false, 0);
        
        var gotText : String = Util.nextTokenGathered(xpp, XmlEventType.TEXT, false);
        //System.out.println(getClass()+" text="+printable(text)+" got="+printable(gotText));
        Assert.equals(Util.printable(text), Util.printable(gotText));
        
        //xpp.nextToken();
        Util.checkParserStateNs(xpp, 2, XmlEventType.COMMENT, null, 2, null, null, comment, false, -1);
        
        xpp.nextToken();
        Util.checkParserStateNs8(xpp, 2, XmlEventType.END_TAG, 2, "uri1", "bar", false, -1);
        
        xpp.nextToken();
        Util.checkParserStateNs8(xpp, 1, XmlEventType.END_TAG, 0, "", "foo", false, -1);
    
    }
    
    inline private static var ENV = "http://www.w3.org/2002/06/soap-envelope";
    inline private static var ALERTCONTROL = "http://example.org/alertcontrol";
    inline private static var ALERT = "http://example.org/alert";
    inline private static var EXPIRES = "2001-06-22T14:00:00-05:00";
    inline private static var MSG = "Pick up Mary at school at 2pm";
    inline private static var ROLE = "http://www.w3.org/2002/06/soap-envelope/role/ultimateReceiver";
    
    // based on example from SOAP 1.2 spec http://www.w3.org/TR/soap12-part1/
    inline private static var SOAP12 = "<env:Envelope xmlns:env=\""+ENV+"\">"+
        "<env:Header>"+
        "<n:alertcontrol xmlns:n=\""+ALERTCONTROL+"\""+
        " env:mustUnderstand=\"true\""+
        " env:role=\""+ROLE+"\">"+
        "<n:priority>1</n:priority>"+
        "<n:expires>"+EXPIRES+"</n:expires>"+
        "</n:alertcontrol>"+
        "</env:Header>"+
        "<env:Body>"+
        "<m:alert xmlns:m=\""+ALERT+"\" >"+
        "<m:msg>"+MSG+"</m:msg>"+
        "</m:alert>"+
        "</env:Body>"+
        "</env:Envelope>";

    
    private function generateSoapEnvelope3(envPrefix:String,
                                        alertcontrolPrefix:String,
                                        alertPrefix:String) : String
                                        {
        return generateSoapEnvelope(envPrefix, alertcontrolPrefix, alertPrefix,
                                    null, null, null);
    }
    
    private var PROPERTY_SERIALIZER_INDENTATION = "http://xmlpull.org/v1/doc/properties.html#serializer-indentation";
    private var PROPERTY_SERIALIZER_LINE_SEPARATOR = "http://xmlpull.org/v1/doc/properties.html#serializer-line-separator";
    private var FEATURE_SERIALIZER_ATTVALUE_USE_APOSTROPHE = "http://xmlpull.org/v1/doc/features.html#serializer-attvalue-use-apostrophe";
    
    private var serializerIndentationSupported : Bool;
    private var serializerLineSeparatorSupported : Bool;
    private var serializerUseApostropheSupported : Bool;

    
    /**
     * Test optional support pretty printing
     */
    public function testUseApostrophe() : Void
    {
        var ser : XmlSerializer = factory.newSerializer();
        try
        {
            ser.setFeature(FEATURE_SERIALIZER_ATTVALUE_USE_APOSTROPHE, true);
        } catch (ex:Dynamic)
        {
            // ignore test if optional property not supported
            return;
        }
        
        var useApost : Bool = ser.getFeature(FEATURE_SERIALIZER_ATTVALUE_USE_APOSTROPHE);
        Assert.equals(true, useApost);
        checkAttributeQuot(true, ser);
        
        ser.setFeature(FEATURE_SERIALIZER_ATTVALUE_USE_APOSTROPHE, false);
        useApost = ser.getFeature(FEATURE_SERIALIZER_ATTVALUE_USE_APOSTROPHE);
        Assert.equals(false, useApost);
        
        checkAttributeQuot(false, ser);
        useApost = ser.getFeature(FEATURE_SERIALIZER_ATTVALUE_USE_APOSTROPHE);
        Assert.equals(false, useApost);
        
        ser.setFeature(FEATURE_SERIALIZER_ATTVALUE_USE_APOSTROPHE, true);
        useApost = ser.getFeature(FEATURE_SERIALIZER_ATTVALUE_USE_APOSTROPHE);
        Assert.equals(true, useApost);
        checkAttributeQuot(true, ser);
        
        checkAttributeQuotMix(ser);
    }
    
    /**
     * Check that attribute was quoted correctly
     */
    private function checkAttributeQuot(useApostrophe:Bool, ser:XmlSerializer) : Void
    {
        var sw : BytesOutput = new BytesOutput();
        ser.setOutput(sw);
        
        ser.startTag("", "test");
        ser.attribute(null, "att", "value");
        ser.endTag("", "test");
        ser.endDocument();
        
        var s : String = sw.getBytes().toString();
        if(useApostrophe)
        {
            Assert.isTrue(s.indexOf("'value'") !=-1);
        } else
        {
            Assert.isTrue(s.indexOf("\"value\"") !=-1);
        }
        
        // some validaiton of serialized XML
        var pp : XmlPullParser = factory.newPullParser();
        pp.setInput(new StringInput(s));
        pp.nextTag();
        pp.require(XmlEventType.START_TAG, null, "test");
        Assert.equals("value", pp.getAttributeValue(XmlConstants.NO_NAMESPACE, "att"));
        pp.nextTag();
        pp.require(XmlEventType.END_TAG, null, "test");
    }
    
    /**
     * Check that attribute quotations can be changed _during_ serialization
     */
    private function checkAttributeQuotMix(ser:XmlSerializer) : Void
    {
        var sw : BytesOutput = new BytesOutput();
        ser.setOutput(sw);
        
        ser.startTag("", "test");
        ser.attribute(null, "att", "value");
        ser.setFeature(FEATURE_SERIALIZER_ATTVALUE_USE_APOSTROPHE, true);
        ser.attribute(null, "attA", "valueA");
        ser.setFeature(FEATURE_SERIALIZER_ATTVALUE_USE_APOSTROPHE, false);
        ser.attribute(null, "attQ", "valueQ");
        ser.endTag("", "test");
        ser.endDocument();
        
        var s : String = sw.getBytes().toString();
        Assert.isTrue(s.indexOf("'valueA'") !=-1);
        Assert.isTrue(s.indexOf("\"valueQ\"") !=-1);
        
        // some validaiton of serialized XML
        var pp : XmlPullParser = factory.newPullParser();
        pp.setInput(new StringInput(s));
        pp.nextTag();
        pp.require(XmlEventType.START_TAG, null, "test");
        Assert.equals("value", pp.getAttributeValue(XmlConstants.NO_NAMESPACE, "att"));
        Assert.equals("valueA",  pp.getAttributeValue(XmlConstants.NO_NAMESPACE, "attA"));
        Assert.equals("valueQ", pp.getAttributeValue(XmlConstants.NO_NAMESPACE, "attQ"));
        pp.nextTag();
        pp.require(XmlEventType.END_TAG, null, "test");
    }

    
    public function testIndentation() : Void
    {
        var ser : XmlSerializer = factory.newSerializer();
        try
        {
            ser.setProperty(PROPERTY_SERIALIZER_INDENTATION, " ");
        } catch (ex:Dynamic)
        {
            // ignore test if optional property not supported
            return;
        }
        
        var sw : BytesOutput = new BytesOutput();
        ser.setOutput(sw);
        
        ser.startTag("", "S1");
        ser.startTag("", "S2");
        ser.text("T");
        ser.endTag("", "S2");
        ser.startTag("", "M2");
        ser.startTag("", "M3");
        ser.endTag("", "M3");
        ser.endTag("", "M2");
        ser.endTag("", "S1");
        ser.endDocument();
        
        var xml : String = sw.getBytes().toString();
        //System.out.println(getClass()+" xml="+xml);
        checkFormatting(" ", 0, "\n", "<S1", xml);
        checkFormatting(" ", 1, "\n", "<S2", xml);
        checkFormatting("T", 1, null, "</S2", xml); //special case set that no indent but content
        checkFormatting(" ", 1, "\n", "<M2", xml);
        checkFormatting(" ", 2, "\n", "<M3", xml);
        checkFormatting(" ", 1, "\n", "</M2", xml);
        checkFormatting(" ", 0, "\n", "</S1", xml);
        
        //TODO check if line separators property is supported ...
    }
    
    private function checkFormatting(indent:String, level:Int, lineSeparator:String,
                                 s:String, xml:String) : Void
                                 {
        // check that s is on output XML
        var pos : Int = xml.indexOf(s);
        Assert.isTrue(pos >= 0);
        // check that indent string is used at level
        for (i in 0...level)
        {
            //for (j:Int = indent.length() - 1; j >= 0 ; j--)
            for (j in [for (k in 0...indent.length) indent.length - 1 - k])
            {
                --pos;
                if(pos < 0)
                {
                    Assert.fail("not enough indent for "+Util.printable(s)+" in "+Util.printable(xml));
                }
                var indentCh : Int = indent.charCodeAt(j);
                var ch : Int = xml.charCodeAt(pos);
                Assert.equals( Util.printableChar(indentCh), Util.printableChar(ch));
            }
        }
        // check that indent is of exact size and line ending is as expected
        if(pos > 0)
        {
            --pos;
            var ch : Int = xml.charCodeAt(pos);
            if(lineSeparator != null)
            {
                //for (i:Int = lineSeparator.length() - 1; i >=0 ; i--)
                for (i in [for (k in 0...lineSeparator.length) lineSeparator.length - 1 - k])
                {
                    var lineSepCh : Int = lineSeparator.charCodeAt(i);
                    Assert.equals( Util.printableChar(lineSepCh), Util.printableChar(ch));
                    --pos;
                    ch = xml.charCodeAt(pos);
                
                }
            } else
            {
                var indentCh : Int = indent.charCodeAt(indent.length - 1);
                Assert.isTrue( indentCh != ch);
            }
        }
    }
    
    public function testLineSeparator() : Void
    {
        var ser : XmlSerializer = factory.newSerializer();
        try
        {
            ser.setProperty(PROPERTY_SERIALIZER_LINE_SEPARATOR, "\n");
        } catch (ex:Dynamic)
        {
            // ignore test if optional property not supported
            return;
        }
    }
    
    /** generate SOAP 1.2 envelope
     try to use indentation
     
     and check automtic namespace prefix declaration
     and auto-generation of prefixes
     
     */
    private function generateSoapEnvelope(envPrefix:String,
                                        alertcontrolPrefix:String,
                                        alertPrefix:String,
                                        attvalueUseApostrophe:Null<Bool>,
                                        indentation:String,
                                        lineSeparator:String
) : String
        
        {
        var ser : XmlSerializer = factory.newSerializer();
        var sw : BytesOutput = new BytesOutput();
        ser.setOutput(sw);
        
        if(attvalueUseApostrophe !=null)
        {
            try {
                ser.setFeature(FEATURE_SERIALIZER_ATTVALUE_USE_APOSTROPHE,
                               attvalueUseApostrophe);
                serializerUseApostropheSupported = true;
            } catch (ex:Dynamic)
            {
                // ignore if optional feature not supported
            }
        }
        if(indentation !=null)
        {
            try {
                ser.setProperty(PROPERTY_SERIALIZER_INDENTATION, indentation);
                serializerIndentationSupported = true;
            } catch (ex:Dynamic)
            {
                // ignore if optional property not supported
            }
        }
        if(lineSeparator !=null)
        {
            try {
                ser.setProperty(PROPERTY_SERIALIZER_LINE_SEPARATOR, lineSeparator);
                serializerLineSeparatorSupported = true;
            } catch (ex:Dynamic)
            {
                // ignore if optional property not supported
            }
        }
        
        // all comments etc
        ser.startDocument(null, true);
        
        if(envPrefix != null) ser.setPrefix(envPrefix, ENV);
        ser.startTag(ENV, "Envelope");
        ser.startTag(ENV, "Header");
        
        if(alertcontrolPrefix != null) ser.setPrefix(alertcontrolPrefix, ALERTCONTROL);
        ser.startTag(ALERTCONTROL, "alertcontrol");
        ser.attribute(ENV, "mustUnderstand", "true");
        ser.attribute(ENV, "role", ROLE);
        
        ser.startTag(ALERTCONTROL, "priority");
        ser.text("1");
        ser.endTag(ALERTCONTROL, "priority");
        
        ser.startTag(ALERTCONTROL, "expires");
        ser.text(EXPIRES);
        ser.endTag(ALERTCONTROL, "expires");
        
        ser.endTag(ALERTCONTROL, "alertcontrol");
        
        ser.endTag(ENV, "Header");
        
        ser.startTag(ENV, "Body");
        
        if(alertPrefix != null) ser.setPrefix(alertPrefix, ALERT);
        ser.startTag(ALERT, "alert");
        
        ser.startTag(ALERT, "msg");
        ser.text(MSG);
        ser.endTag(ALERT, "msg");
        
        ser.endTag(ALERT, "alert");

        
        ser.endTag(ENV, "Body");
        
        ser.endTag(ENV, "Envelope");
        
        ser.endDocument();
        
        var s : String = sw.getBytes().toString();
        
        return s;
    }
    
    public function doTestSetPrefix(prefix:String) : Void
    {
        var ser : XmlSerializer = factory.newSerializer();
        var sw : BytesOutput = new BytesOutput();
        ser.setOutput(sw);
        var NS = "http://example.com/test";
        ser.setPrefix(prefix, NS);
        ser.startTag(NS, "foo");
        ser.endDocument();
        
        var serialized : String = sw.getBytes().toString();
        xpp.setInput(new StringInput(serialized));
        
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        var expectedPrefix : String = (prefix == "" ? null : prefix);
        var expectedCount = (expectedPrefix == null ? 0 : 1);
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, expectedPrefix, expectedCount, NS, "foo", null, xpp.isEmptyElementTag() /*empty*/, 0);
    }
    
    public function testSetPrefixes() : Void
    {
        doTestSetPrefix("ns");
        doTestSetPrefix("");
        doTestSetPrefix(null);
    }
    
    /**
     * Testing for case described in http://www.extreme.indiana.edu/bugzilla/show_bug.cgi?id=241
     */
    public function doTestAttrPrefix(prefix:String, extraPrefix:Bool) : Void
    {
        var ser : XmlSerializer = factory.newSerializer();
        //System.err.println(getClass()+" ser="+ser.getClass());
        var sw : BytesOutput = new BytesOutput();
        ser.setOutput(sw);
        var NS = "http://example.com/test";
        ser.setPrefix(prefix, NS);
        if(extraPrefix) ser.setPrefix("p1", NS);
        ser.startTag(NS, "foo");
        ser.attribute(NS, "attr", "aar");
        ser.endDocument();
        
        var serialized : String = sw.getBytes().toString();
        xpp.setInput(new StringInput(serialized));
        
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        var expectedPrefix : String = (prefix != null && prefix.length == 0) ? null : prefix;
        var nsCount : Int = 1;
        if(extraPrefix && expectedPrefix != null) ++nsCount;
        if(extraPrefix) expectedPrefix = "p1";

        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, expectedPrefix, nsCount, NS, "foo", null, xpp.isEmptyElementTag() /*empty*/, 1);

        Util.checkAttribNs5(xpp, 0, NS, "attr", "aar");
    }
    
    public function testAttrPrefixes() : Void
    {
        doTestAttrPrefix("ns", false);
        doTestAttrPrefix("", false);
        doTestAttrPrefix(null, false);
        doTestAttrPrefix("ns", true);
        doTestAttrPrefix("", true);
        doTestAttrPrefix(null, true);
    }

    
    /** setPrefix check that prefix is not duplicated ... */
    public function brokentestSetPrefixAdv() : Void
    {
        //TODO check redeclaring defult namespace

        
        checkTestSetPrefixSoap(SOAP12);
        checkTestSetPrefixSoap(generateSoapEnvelope3("env", "n", "m"));
        checkTestSetPrefixSoap(generateSoapEnvelope3(null, null, "m"));
        checkTestSetPrefixSoap(generateSoapEnvelope3("env", null, "m"));
        checkTestSetPrefixSoap(generateSoapEnvelope3("env", "", ""));

        
        var generated : String = generateSoapEnvelope3("", "n", "m");
        //System.err.println(getClass()+" generated="+generated);
        
        // 1 is for one extra namespace must be added to declare xmlns namespace
        //    for attrbute mustUnderstan in SOAP-ENV namespace
        trace(generated);
        checkTestSetPrefixSoap3(generated, 0,false);

        
        checkTestSetPrefixSoap3(generateSoapEnvelope3("", null, "m"),0,false);
        
        //check optional pretty printing
        checkTestSetPrefixSoap(generateSoapEnvelope("env", "n", "m", false, null, null));
        checkTestSetPrefixSoap(generateSoapEnvelope("env", "n", "m", true, null, null));
        checkTestSetPrefixSoapA2(generateSoapEnvelope("env", "n", "m", null, " ", null), true);
        checkTestSetPrefixSoapA2(generateSoapEnvelope("env", "n", "m", null, "\t", null), true);
        checkTestSetPrefixSoapA2(generateSoapEnvelope("env", "n", "m", null, "    ", null), true);
        var s : String = generateSoapEnvelope("env", "n", "m", true, " ", "\n");
        //System.out.println(getClass()+" envelope="+generateSoapEnvelope("", "n", "m"));
        checkTestSetPrefixSoapA2(s, true);
    }
    
    /** check that ti is possible to select which prefix should be used for namespace
     * For more details check http://www.extreme.indiana.edu/bugzilla/show_bug.cgi?id=169
     */
    public function testSetPrefixPreferencesAll() : Void
    {
        doTestSetPrefixPreferences("", null);
        doTestSetPrefixPreferences("saml", "saml");
        
        // check case when prefix in Infoset is invalid -- prefix preoprty is optional after all
        // http://www.w3.org/TR/xml-infoset/#infoitem.element
        doTestSetPrefixPreferences("sample", "saml");
        
        doTestSetPrefixPreferences("", null);
        doTestSetPrefixPreferences("saml", "saml");
    
    }
    
    private function doTestSetPrefixPreferences(preferredPrefix:String, expectedPrefix:String) : Void
    {
        
        var ser : XmlSerializer = factory.newSerializer();
        var sw : BytesOutput = new BytesOutput();
        ser.setOutput(sw);
        //<Assertion xmlns="urn:oasis:names:tc:SAML:1.0:assertion"
        var NS = "urn:oasis:names:tc:SAML:1.0:assertion";
        var deferred : Bool = false;
        if(""==(preferredPrefix)) {
            deferred = true;
        } else
        {
            ser.setPrefix("", NS);
        }
        if("saml"==(preferredPrefix)) {
            deferred = true;
        } else
        {
            ser.setPrefix("saml", NS);
        
        }
        ser.setPrefix("samlp", "urn:oasis:names:tc:SAML:1.0:protocol");
        if(deferred)
        {
            ser.setPrefix(preferredPrefix, NS);
        }
        ser.startTag(NS, "Assertion");
        var serAtt : Bool = !(""==(preferredPrefix));
        if(serAtt)
        {
            ser.attribute(NS, "test", "1");
        }
        ser.endTag(NS, "Assertion");
        ser.endDocument();
        
        sw.close();
        var serialized : String = sw.getBytes().toString();

        xpp.setInput(new StringInput(serialized));
        
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        //String expectedPrefix = (prefix2 != null && prefix2.length() == 0) ? null : prefix2;
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, expectedPrefix, 2,
                           NS, "Assertion", null, xpp.isEmptyElementTag() /*empty*/, serAtt ? 1 : 0);
        if(serAtt)
        {
            Assert.equals(expectedPrefix, xpp.getAttributePrefix(0)); //NS, "test
        }
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, expectedPrefix, 2,
                           NS, "Assertion", null, false, -1);
    
    }
    
    private function checkTestSetPrefixSoap(soapEnvelope:String) : Void
    {
        checkTestSetPrefixSoap3(soapEnvelope, 0, false);
    }
    
    private function checkTestSetPrefixSoapA2(soapEnvelope:String, indented:Bool) : Void
    {
        checkTestSetPrefixSoap3(soapEnvelope, 0, indented);
    }
    
    // run test using checkParserStateNs()
    private function checkTestSetPrefixSoap3(soapEnvelope:String, extraNs:Int, indented:Bool) : Void {

        var nsc = 0;
        
        //compare that XML representation of soapEnvelope is as desired
        if(!indented)
        {
            assertXmlEquals(SOAP12, soapEnvelope);
        }

        xpp.setInput(new StringInput(soapEnvelope));
        
        xpp.setFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES, true);
        
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        if (xpp.getPrefix() != null) nsc++;
        Util.checkParserStateNs8(xpp, 1, XmlEventType.START_TAG, nsc, ENV, "Envelope", false /*empty*/, 0);
        xpp.nextTag();
        Util.checkParserStateNs8(xpp, 2, XmlEventType.START_TAG, nsc, ENV, "Header", false /*empty*/, 0);
        xpp.nextTag();
        Util.checkParserStateNs8(xpp, 3, XmlEventType.START_TAG,
                           2+extraNs, ALERTCONTROL, "alertcontrol", false /*empty*/, 2);
        Util.Util.checkAttribNs5(xpp, 0, ENV, "mustUnderstand", "true");
        Util.Util.checkAttribNs5(xpp, 1, ENV, "role", ROLE);
        
        xpp.nextTag();
        Util.checkParserStateNs8(xpp, 4, XmlEventType.START_TAG, 2+extraNs, ALERTCONTROL, "priority", false /*empty*/, 0);
        var text : String = xpp.nextText();
        Assert.equals("1", text);
        xpp.nextTag();
        Util.checkParserStateNs8(xpp, 4, XmlEventType.START_TAG, 2+extraNs, ALERTCONTROL, "expires", false /*empty*/, 0);
        text = xpp.nextText();
        Assert.equals(EXPIRES, text);
        xpp.nextTag();
        Util.checkParserStateNs8(xpp, 3, XmlEventType.END_TAG, 2+extraNs, ALERTCONTROL, "alertcontrol", false, -1);
        xpp.nextTag();
        Util.checkParserStateNs8(xpp, 2, XmlEventType.END_TAG, 1, ENV, "Header", false, -1);
        xpp.nextTag();
        Util.checkParserStateNs8(xpp, 2, XmlEventType.START_TAG, 1, ENV, "Body", false /*empty*/, 0);
        
        xpp.nextTag();
        Util.checkParserStateNs8(xpp, 3, XmlEventType.START_TAG, 2, ALERT, "alert", false /*empty*/, 0);
        xpp.nextTag();
        Util.checkParserStateNs8(xpp, 4, XmlEventType.START_TAG, 2, ALERT, "msg", false /*empty*/, 0);
        text = xpp.nextText();
        Assert.equals(MSG, text);
        xpp.nextTag();
        Util.checkParserStateNs8(xpp, 3, XmlEventType.END_TAG, 2, ALERT, "alert", false, -1);
        xpp.nextTag();
        Util.checkParserStateNs8(xpp, 2, XmlEventType.END_TAG, 1, ENV, "Body", false, -1);
        xpp.nextTag();
        Util.checkParserStateNs8(xpp, 1, XmlEventType.END_TAG, 1, ENV, "Envelope", false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
        
        checkTestSetPrefixSoap2(soapEnvelope);
    }
    
    // run test using parser.require()
    private function checkTestSetPrefixSoap2(soapEnvelope:String) : Void
    {
        xpp.setInput(new StringInput(soapEnvelope));
        xpp.setFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES, true);
        
        xpp.require(XmlEventType.START_DOCUMENT, null, null);
        
        xpp.next(); // essentially moveToContent()
        xpp.require(XmlEventType.START_TAG, ENV, "Envelope");
        
        xpp.nextTag();
        xpp.require(XmlEventType.START_TAG, ENV, "Header");
        
        xpp.nextTag();
        xpp.require(XmlEventType.START_TAG, ALERTCONTROL, "alertcontrol");
        var mustUderstand : String = xpp.getAttributeValue(ENV, "mustUnderstand");
        Assert.equals("true", mustUderstand);
        var role : String = xpp.getAttributeValue(ENV, "role");
        Assert.equals(ROLE, role);
        
        xpp.nextTag();
        xpp.require(XmlEventType.START_TAG, ALERTCONTROL, "priority");
        var text : String = xpp.nextText();
        Assert.equals("1", text);
        //Integer.parseInt(text);
        
        xpp.nextTag();
        xpp.require(XmlEventType.START_TAG, ALERTCONTROL, "expires");
        text = xpp.nextText();
        Assert.equals(EXPIRES, text);
        
        xpp.nextTag();
        xpp.require(XmlEventType.END_TAG, ALERTCONTROL, "alertcontrol");
        
        xpp.nextTag();
        xpp.require(XmlEventType.END_TAG, ENV, "Header");
        
        xpp.nextTag();
        xpp.require(XmlEventType.START_TAG, ENV, "Body");
        
        xpp.nextTag();
        xpp.require(XmlEventType.START_TAG, ALERT, "alert");
        
        xpp.nextTag();
        xpp.require(XmlEventType.START_TAG, ALERT, "msg");
        
        text = xpp.nextText();
        Assert.equals(MSG, text);
        
        xpp.nextTag();
        xpp.require(XmlEventType.END_TAG, ALERT, "alert");
        
        xpp.nextTag();
        xpp.require(XmlEventType.END_TAG, ENV, "Body");
        
        xpp.nextTag();
        xpp.require(XmlEventType.END_TAG, ENV, "Envelope");
        
        xpp.next();
        xpp.require(XmlEventType.END_DOCUMENT, null, null);
    }

    
    public function testConflictingDefaultNs() : Void
    {
        var ser : XmlSerializer = factory.newSerializer();
        
        var baos : BytesOutput = new BytesOutput();
        ser.setOutput(baos, "UTF-8");
        
        ser.setPrefix("", "namesp");
        ser.setPrefix("ns1", "namesp1");
        ser.setPrefix("ns2", "namesp2");
        try
        {
            ser.startTag("", "foo");
            Assert.fail("exception was expected when default namespace can not be declared");
        } catch(ex:XmlPullParserException)
        {
            //
        }
    }

    
    public function testMultipleOverlappingNamespaces() : Void
    {
        var ser : XmlSerializer = factory.newSerializer();
        
        //<section xmlns='urn:com:books-r-us'>
        //  <!-- 2 -->   <title>Book-Signing Event</title>
        //  <!-- 3 -->   <signing>
        //  <!-- 4 -->     <author title="Mr" name="Vikram Seth" />
        //  <!-- 5 -->     <book title="A Suitable Boy" price="$22.95" />
        //               </signing>
        //             </section>

        
        // check namespaces generation with explicit prefixes
        
        //        byte[] binput = ("<foo xmlns='namesp' xmlns:ns1='namesp1' xmlns:ns2='namesp2'>"+
        //                             "<ns1:bar xmlns:ns1='x1' xmlns:ns3='namesp3' xmlns='n1'>"+
        //                             "<ns2:gugu a1='v1' ns2:a2='v2' xml:lang='en' ns1:a3=\"v3\"/>"+
        //                             "<baz xmlns:ns1='y1'></baz>"+
        //                             "</ns1:bar></foo>").getBytes("US-ASCII");
        var baos : BytesOutput = new BytesOutput();
        ser.setOutput(baos, "UTF-8");
        
        ser.startDocument(null, null);
        ser.setPrefix("", "namesp");
        ser.setPrefix("ns1", "namesp1");
        ser.setPrefix("ns2", "namesp2");
        ser.startTag("namesp", "foo");
        
        ser.setPrefix("ns1", "x1");
        ser.setPrefix("ns3", "namesp3");
        ser.setPrefix("", "namesp1");
        ser.startTag("x1", "bar");
        
        ser.startTag("namesp2", "gugu");
        ser.attribute("", "a1", "v1");
        ser.attribute("namesp2", "a2", "v2");
        ser.attribute("http://www.w3.org/XML/1998/namespace", "lang", "en");
        ser.attribute("x1", "a3", "v3");
        
        ser.endTag("namesp2", "gugu");
        
        ser.setPrefix("ns1", "y1");
        ser.startTag("namesp1", "baz");
        
        ser.endTag("namesp1", "baz");
        
        ser.endTag("x1", "bar");
        
        ser.endTag("namesp", "foo");
        ser.endDocument();

        
        var binput = baos.getBytes();

        Assert.equals("<?xml version='1.0' encoding='UTF-8' ?><foo xmlns=\"namesp\" xmlns:ns1=\"namesp1\" xmlns:ns2=\"namesp2\"><ns1:bar xmlns:ns1=\"x1\" xmlns:ns3=\"namesp3\" xmlns=\"namesp1\"><ns2:gugu a1=\"v1\" ns2:a2=\"v2\" xml:lang=\"en\" ns1:a3=\"v3\" /><baz xmlns:ns1=\"y1\" /></ns1:bar></foo>", binput.toString());
        
        //System.out.println(getClass().getName()+"serialized="+new String(binput, "US-ASCII"));
        
    }
    
    private function assertXmlEquals(expectedXml:String, actualXml:String) : Void
        
        {
        var expect : XmlPullParser = factory.newPullParser();
        expect.setInput(new StringInput(expectedXml));
        var actual : XmlPullParser = factory.newPullParser();
        actual.setInput(new StringInput(actualXml));
        while(true)
        {
            expect.next();
            actual.next();
            assertXml("inconsistent event type", expect, actual,
                      XmlEventType.getName( expect.getEventType() ),
                      XmlEventType.getName( actual.getEventType() )
);
            if(expect.getEventType() == XmlEventType.END_DOCUMENT)
            {
                break;
            }
            if(expect.getEventType() == XmlEventType.START_TAG
                   || expect.getEventType() == XmlEventType.END_TAG)
                   {
                assertXml("tag names", expect, actual,
                          expect.getName(), actual.getName());
                assertXml("tag namespaces", expect, actual,
                          expect.getNamespace(), actual.getNamespace());
                if(expect.getEventType() == XmlEventType.START_TAG)
                {
                    // check consisteny of attributes -- allow them to be in any order
                    var expectAttrCount : Int = expect.getAttributeCount();
                    assertXml("attributes count", expect, actual,
                              ""+expectAttrCount, ""+actual.getAttributeCount());
                    for (i in 0...expectAttrCount)
                    {
                        var expectAttrNamespace : String = expect.getAttributeNamespace(i);
                        var expectAttrName : String = expect.getAttributeName(i);
                        var expectAttrType : String = expect.getAttributeType(i);
                        var expectAttrValue : String = expect.getAttributeValueByIndex(i);
                        var expectAttrDefault : Bool = expect.isAttributeDefault(i);
                        
                        // find this attribute actual position
                        var actualPos : Int = -1;
                        for (j in 0...expectAttrCount)
                        {
                            if(expectAttrNamespace == actual.getAttributeNamespace(j)
                                   && expectAttrName == actual.getAttributeName(j))
                                   {
                                actualPos = j;
                                break;
                            }
                        }
                        var expectN : String = expectAttrNamespace+":"+expectAttrName;
                        if(actualPos == -1)
                        {
                            trace("expected:\n"+expectedXml
                                                   +"\nactual:\n"+actualXml);
                            Assert.fail("could not find expected attribute "+expectN
                                     +" actual parser "+actual.getPositionDescription());
                        }
                        
                        //and compare ...
                        assertXml("attribute "+expectN+" namespace", expect, actual,
                                  expectAttrNamespace, actual.getAttributeNamespace(actualPos));
                        assertXml("attribute "+expectN+" name", expect, actual,
                                  expectAttrName, actual.getAttributeName(actualPos));
                        assertXml("attribute "+expectN+" type", expect, actual,
                                  expectAttrType, actual.getAttributeType(actualPos));
                        assertXml("attribute "+expectN+" value", expect, actual,
                                  expectAttrValue, actual.getAttributeValueByIndex(actualPos));
                        assertXml("attribute "+expectN+" default", expect, actual,
                                  ""+expectAttrDefault, ""+actual.isAttributeDefault(actualPos));
                    }
                }
            } else if(expect.getEventType() == XmlEventType.TEXT)
            {
                assertXml("text content", expect, actual,
                          expect.getText(), actual.getText());
            } else
            {
                Assert.fail("unexpected event type "+expect.getEventType()+" "+expect.getPositionDescription());
            }
            //System.err.print(".");
        }
        //System.err.println("\nOK");
    }
    
    private static function assertXml(formatted:String,
                                  pExpected:XmlPullParser, pActual:XmlPullParser,
                                  expected:String, actual:String
) : Void
                                 {
        if((expected != null && expected != actual)
               || (expected == null && actual != null))
               {
            Assert.fail(formatted
                     +" expected:<"+expected+"> but was:<"+actual+">"
                     +" (expecte parser position:"+pExpected.getPositionDescription()
                     +" and actual parser positon:"+pActual.getPositionDescription()

);
        }
    }
}

