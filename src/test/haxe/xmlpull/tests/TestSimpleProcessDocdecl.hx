/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

/* -*-             c-basic-offset: 4; indent-tabs-mode: nil; -*-  //------100-columns-wide------>|*/
// for license see accompanying LICENSE_TESTS.txt file (available also at http://www.xmlpull.org)

package xmlpull.tests;

import utest.Assert;



import haxe.io.StringInput;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;

import xmlpull.XmlConstants.*;

/**
 * Test FEATURE_PROCESS_DOCDECL  (when supported)
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestSimpleProcessDocdecl 
{
    private var factory : XmlPullParserFactory;
    
    
    public function new()
    {
        
    }
    
    private function setup() : Void
    {
        factory = Util.newFactory();
        Assert.equals(false, factory.isNamespaceAware());
        Assert.equals(false, factory.isValidating());
        //System.out.println("factory="+factory);
    }
    
    public function testProcessDocdecl() : Void
    {
        var xpp : XmlPullParser = factory.newPullParser();
        try
        {
            xpp.setFeature(XmlConstants.FEATURE_PROCESS_DOCDECL, true);
        } catch(ex:XmlPullParserException)
        {
            return;
        }
        // setting validation MUST enables also PROCESS_DOCDECL
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_DOCDECL));
        // default is to have non-validating parser
        Assert.equals(false, xpp.getFeature(XmlConstants.FEATURE_VALIDATION));
        
        //http://www.w3.org/TR/REC-xml#NT-extSubsetDecl
        // minimum validation
        var XML_MIN_PROLOG = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone='yes' ?>\n"+
            "<!DOCTYPE greeting [\n"+
            "<!ENTITY name \"world\">\n"+
            "]>\n";
        
        var XML_MIN_VALID : String = XML_MIN_PROLOG+
            "<greeting>Hello, &name;!</greeting>\n";
        
        xpp.setInput(new StringInput( XML_MIN_VALID));
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        Assert.isNull(xpp.getProperty(PROPERTY_XMLDECL_VERSION));
        Assert.isNull(xpp.getProperty(PROPERTY_XMLDECL_STANDALONE));
        Assert.isNull(xpp.getProperty(PROPERTY_XMLDECL_CONTENT));
        
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "greeting", null, false/*empty*/, 0);
        
        //XMLDecl support is required when PROCESS DOCDECL enabled
        Assert.equals("1.0", xpp.getProperty(PROPERTY_XMLDECL_VERSION));
        Assert.equals(true, xpp.getProperty(PROPERTY_XMLDECL_STANDALONE));
        
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null, "Hello, world!", false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "greeting", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);


        
        //AND WRONG
        var XML_MIN_INVALID : String = XML_MIN_PROLOG+
            "<greet>Hello, &world;!</greet>\n";
        xpp.setInput(new StringInput( XML_MIN_INVALID));
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "greet", null, false/*empty*/, 0);
        try
        {
            xpp.next();
            Assert.fail("exception was expected of next() for undeclared entity");
        } catch(ex:XmlPullParserException) {}
    
    }

}

