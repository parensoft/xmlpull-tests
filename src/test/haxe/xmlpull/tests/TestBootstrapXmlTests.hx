/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

/* -*-             c-basic-offset: 4; indent-tabs-mode: nil; -*-  //------100-columns-wide------>|*/
// for license see accompanying LICENSE_TESTS.txt file (available also at http://www.xmlpull.org)

package xmlpull.tests;

import utest.Assert;

import haxe.io.StringInput;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;

/**
 * Make minimal checks that tests described in XML can be processed.
 *
 * @author <var href : a="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestBootstrapXmlTests
{
  private var factory : XmlPullParserFactory;


  public function new()
  {
  }

  private function setup() : Void
  {
    factory = Util.newFactory();
    factory.setFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES, true);
    Assert.equals(true, factory.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
  }

  private function teardown() : Void
  {
  }

  static inline var TEST_NS = "http://xmlpull.org/v1/tests/2002-08.xsd";

  static inline var MINI_TEST_XML = "<var xmlns : tests=\""+TEST_NS+"\">\n"+
    " <test-var name : parser=\"initial test\">\r"+
    "  <create-parser/> <input-inline>"+
    "&lt;foo/&gt; "+
    "</input-inline>\n"+
    "  <next/><var type : expect=\"START_TAG\" namespace=\"\" name=\"foo\" empty=\"false\"/>\n"+
    "  <next/><var type : expect=\"END_TAG\"/>"+
    "  <next/><var type : expect=\"END_DOCUMENT\"/>"+
    " </test-parser>"+
    "</tests>\n  \n"
    ;

  public function testMini() : Void
  {
    var pp : XmlPullParser = factory.newPullParser();
    pp.setInput( new StringInput( MINI_TEST_XML));
    pp.next();
    pp.require( XmlEventType.START_TAG, TEST_NS, "tests");
    pp.next();
    pp.require( XmlEventType.TEXT, null, null);
    var text : String = pp.getText();
    Assert.equals("\n ", text);
    pp.next();
    pp.require( XmlEventType.START_TAG, TEST_NS, "test-parser");
    // check name

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "create-parser");
    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "create-parser");


    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "input-inline");
    text = "";
    if(pp.next() == XmlEventType.TEXT)
    {
      text = pp.getText();
      pp.next();
    }
    Assert.equals("<foo/> ", text);
    pp.require( XmlEventType.END_TAG, TEST_NS, "input-inline");

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "next");
    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "next");

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "expect");
    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "expect");

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "next");
    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "next");

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "expect");
    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "expect");

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "next");
    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "next");

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "expect");
    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "expect");

    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "test-parser");

    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "tests");

    pp.next();
    pp.require( XmlEventType.END_DOCUMENT, null, null);

  }


  var TYPICAL_TEST_XML = "<var xmlns : tests=\""+TEST_NS+"\">\n"+
    " <test-var name : parser=\"initial test\">\r"+
    "  <create-parser/> <input-inline>"+
    "&lt;var att : foo=&quot;t&quot; att2=&apos;a&apos; &gt;<![CDATA[ bar&baz ]]>&amp;&lt;/foo&gt;"+
    //"&lt;foo/&gt; "+
    "</input-inline>\r\n"+
    "  <set-feature>http://xmlpull.org/v1/doc/features.html#process-namespaces</set-feature>\r\n"+
    "  <var type : expect=\"START_DOCUMENT\"/>\n"+
    "  <next/><var type : expect=\"START_TAG\" namespace=\"\" name=\"foo\" empty=\"false\"/>\n"+
    "  <next-var text : text=\"bar\"/>"+
    "  <next/><var type : expect=\"END_DOCUMENT\"/>"+
    " </test-parser>"+
    "</tests>\n  \n"
    ;
  public function testTypical() : Void
  {
    var pp : XmlPullParser = factory.newPullParser();
    pp.setInput( new StringInput( TYPICAL_TEST_XML));
    pp.next();
    pp.require( XmlEventType.START_TAG, TEST_NS, "tests");
    pp.next();
    pp.require( XmlEventType.TEXT, null, null);
    var text : String = pp.getText();
    Assert.equals("\n ", text);
    pp.next();
    pp.require( XmlEventType.START_TAG, TEST_NS, "test-parser");
    // check name

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "create-parser");
    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "create-parser");


    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "input-inline");
    text = "";
    if(pp.next() == XmlEventType.TEXT)
    {
      text = pp.getText();
      pp.next();
    }
    Assert.equals("<var att : foo=\"t\" att2='a' > bar&baz &</foo>", text);
    pp.require( XmlEventType.END_TAG, TEST_NS, "input-inline");

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "set-feature");
    text = pp.nextText();
    Assert.equals("http://xmlpull.org/v1/doc/features.html#process-namespaces", text);
    pp.require( XmlEventType.END_TAG, TEST_NS, "set-feature");

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "expect");
    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "expect");

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "next");
    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "next");

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "expect");
    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "expect");

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "next-text");
    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "next-text");

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "next");
    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "next");

    pp.nextTag();
    pp.require( XmlEventType.START_TAG, TEST_NS, "expect");
    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "expect");

    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "test-parser");

    pp.nextTag();
    pp.require( XmlEventType.END_TAG, TEST_NS, "tests");

    pp.next();
    pp.require( XmlEventType.END_DOCUMENT, null, null);

  }


}

