/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

package xmlpull.tests;

import haxe.io.StringInput;

import utest.Assert;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;
import xmlpull.XmlConstants;
import xmlpull.XmlEventType;

/**
 * Test attribute uniqueness is ensured.
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestAttributes {
    var factory: XmlPullParserFactory;

    public function new() {}

    public function setup() {
        factory = Util.newFactory();

        factory.setNamespaceAware(true);
        Assert.equals(true, factory.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        Assert.equals(false, factory.getFeature(XmlConstants.FEATURE_VALIDATION));
    }

    public function testAttribs() {
        var XML_ATTRS =
            "<event xmlns:xsi='http://www.w3.org/1999/XMLSchema/instance' encodingStyle=\"test\">"+
            "<type>my-event</type>"+
            "<handback xsi:type='ns2:string' xmlns:ns2='http://www.w3.org/1999/XMLSchema' xsi:null='1'/>"+
            "</event>";

        var pp = factory.newPullParser();
        pp.setInput(new StringInput(XML_ATTRS));

        Assert.equals(XmlEventType.START_TAG, pp.next());
        Assert.equals("event", pp.getName());
        Assert.equals(XmlEventType.START_TAG, pp.next());
        Assert.equals("type", pp.getName());
        Assert.equals(XmlEventType.TEXT, pp.next());
        Assert.equals("my-event", pp.getText());
        Assert.equals(pp.next(), XmlEventType.END_TAG);
        Assert.equals("type", pp.getName());
        Assert.equals(XmlEventType.START_TAG, pp.next());
        Assert.equals("handback", pp.getName());
        //Assert.equals(XmlEventType.CONTENT, pp.next());
        //Assert.equals("", pp.readContent());

        var xsiNull = pp.getAttributeValue(
            "http://www.w3.org/1999/XMLSchema/instance", "null");
        Assert.equals("1", xsiNull);

        var xsiType = pp.getAttributeValue(
            "http://www.w3.org/1999/XMLSchema/instance", "type");
        Assert.equals("ns2:string", xsiType);


        var typeName = getQNameLocal(xsiType);
        Assert.equals("string", typeName);
        var typeNS = getQNameUri(pp, xsiType);
        Assert.equals("http://www.w3.org/1999/XMLSchema", typeNS);

        Assert.equals(pp.next(), XmlEventType.END_TAG);
        Assert.equals("handback", pp.getName());
        Assert.equals(pp.next(), XmlEventType.END_TAG);
        Assert.equals("event", pp.getName());

        Assert.equals(pp.next(), XmlEventType.END_DOCUMENT);

    }

    private function getQNameLocal(qname: String): String {
        if(qname == null) return null;

        var pos = qname.indexOf(':');
        return qname.substring(pos + 1);
    }

    private function getQNameUri(pp: XmlPullParser, qname: String): String {
        if(qname == null) return null;

        var pos = qname.indexOf(':');
        if(pos == -1) {
          throw new XmlPullParserException("qname does not have prefix");
        }

        var prefix = qname.substring(0, pos);
        return pp.getNamespaceByPrefix(prefix);
    }

    public function testAttribUniq() {

        var attribsOk =
            "<m:test xmlns:m='Some-Namespace-URI' xmlns:n='Some-Namespace-URI'"+
            " a='a' b='b' m:a='c' n:b='d' n:x='e'"+
            "/>\n"+
            "";

        var duplicateAttribs =
            "<m:test xmlns:m='Some-Namespace-URI' xmlns:n='Some-Namespace-URI'"+
            " a='a' b='b' m:a='a' n:b='b' a='x'"+
            "/>\n"+
            "";

        var duplicateNsAttribs =
            "<m:test xmlns:m='Some-Namespace-URI' xmlns:n='Some-Namespace-URI'"+
            " a='a' b='b' m:a='a' n:b='b' n:a='a'"+
            "/>\n"+
            "";

        var duplicateXmlns =
            "<m:test xmlns:m='Some-Namespace-URI' xmlns:m='Some-Namespace-URI'"+
            ""+
            "/>\n"+
            "";

        var duplicateAttribXmlnsDefault =
            "<m:test xmlns='Some-Namespace-URI' xmlns:m='Some-Namespace-URI'"+
            " a='a' b='b' m:b='b' m:a='x'"+
            "/>\n"+
            "";

        var pp = factory.newPullParser();
        parseOneElement(pp, attribsOk, false);
        Assert.equals("a", pp.getAttributeValue(null, "a"));
        Assert.equals("b", pp.getAttributeValue(null, "b"));
        Assert.equals("c", pp.getAttributeValue(null, "m:a"));
        Assert.equals("d", pp.getAttributeValue(null, "n:b"));
        Assert.equals("e", pp.getAttributeValue(null, "n:x"));

        parseOneElement(pp, attribsOk, true);

        Assert.equals("a", pp.getAttributeValue("","a"));
        Assert.equals("b", pp.getAttributeValue("","b"));
        Assert.equals(null, pp.getAttributeValue("", "m:a"));
        Assert.equals(null, pp.getAttributeValue("", "n:b"));
        Assert.equals(null, pp.getAttributeValue("", "n:x"));

        Assert.equals("c", pp.getAttributeValue("Some-Namespace-URI", "a"));
        Assert.equals("d", pp.getAttributeValue("Some-Namespace-URI", "b"));
        Assert.equals("e", pp.getAttributeValue("Some-Namespace-URI", "x"));

        parseOneElement(pp, duplicateNsAttribs, false);
        parseOneElement(pp, duplicateAttribXmlnsDefault, false);
        parseOneElement(pp, duplicateAttribXmlnsDefault, true);

        var ex = null;

        try {
            parseOneElement(pp, duplicateAttribs, true);
        } catch(rex: XmlPullParserException) {
            ex = rex;
        }
        Assert.notNull(ex);

        ex = null;
        try {
            parseOneElement(pp, duplicateAttribs, false);
        } catch(rex: XmlPullParserException) {
            ex = rex;
        }
        Assert.notNull(ex);

        ex = null;
        try {
            parseOneElement(pp, duplicateXmlns, false);
        } catch(rex: XmlPullParserException) {
            ex = rex;
        }
        Assert.notNull(ex);

        ex = null;
        try {
            parseOneElement(pp, duplicateXmlns, true);
        } catch(rex: XmlPullParserException) {
            ex = rex;
        }
        Assert.notNull(ex);

        ex = null;
        try {
            parseOneElement(pp, duplicateNsAttribs, true);
        } catch(rex: XmlPullParserException) {
            ex = rex;
        }
        Assert.notNull(ex);

        var declaringDefaultEmptyNs = "<m:test xmlns='' xmlns:m='uri'/>";

        parseOneElement(pp, declaringDefaultEmptyNs, false);
        //can redeclare default empty ns
        parseOneElement(pp, declaringDefaultEmptyNs, true);

        var declaringEmptyNs  =
            "<m:test xmlns:m='' />";

        // allowed when namespaces disabled
        parseOneElement(pp, declaringEmptyNs, false);

        // otherwise it is error to declare '' for non-default NS as described in
        //   http://www.w3.org/TR/1999/REC-xml-names-19990114/#ns-decl
        ex = null;
        try {
            parseOneElement(pp, declaringEmptyNs, true);
        } catch(rex: XmlPullParserException) {
            ex = rex;
        }
        Assert.notNull(ex);

    }

    public function testAttribValueNormalization(): Void {
        var XML_ATTRS =
            "<javadoc packagenames=\"${packages}\""+
            " bottom=\"&lt;table width='80%%'&gt;&lt;tr&gt;&lt;td width='50%%'&gt;&lt;p "+
            " align='center'&gt;&lt;a href='http://www.xmlpull.org/'&gt;\" "+
            "/>";

        var pp = factory.newPullParser();
        pp.setInput(new StringInput(XML_ATTRS));

        Assert.equals(XmlEventType.START_TAG, pp.next());
        Assert.equals("javadoc", pp.getName());
        Assert.equals("${packages}", pp.getAttributeValueByIndex(0));
        Assert.equals("${packages}", pp.getAttributeValue("", "packagenames"));
        Assert.equals(
                "<table width='80%%'><tr><td width='50%%'><p  align='center'><a href='http://www.xmlpull.org/'>",
                pp.getAttributeValue("", "bottom")
        );

        //System.out.println(pp.getAttributeValue("", "bottom"));
    }

    private function parseOneElement(
        pp: XmlPullParser,
        buf: String,
        supportNamespaces: Bool) {
        //pp.setInput(buf.toCharArray());
        pp.setInput(new StringInput(buf));
        //pp.setNamespaceAware(supportNamespaces);
        pp.setFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES, supportNamespaces);
        //pp.setAllowedMixedContent(false);
        pp.next();
        //pp.readStartTag(stag);
        if(supportNamespaces) {
            Assert.equals("test", pp.getName());
        } else {
            Assert.equals("m:test", pp.getName());
        }
    }



}

