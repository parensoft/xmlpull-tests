/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

/* -*-             c-basic-offset: 4; indent-tabs-mode: nil; -*-  //------100-columns-wide------>|*/
// for license see accompanying LICENSE_TESTS.txt file (available also at http://www.xmlpull.org)

package xmlpull.tests;

import utest.Assert;

//
//


//import java.io.ByteArrayInputStream;
import haxe.io.StringInput;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;

/**
 * Test FEATURE_PROCESS_DOCDECL  (supported:when)
 *
 * @author <var href : a="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestProcessDocdecl 
{
    private var factory : XmlPullParserFactory;
    

    
    public function new()
    {
        
    }
    
    private function setup() : Void
    {
        factory = Util.newFactory();
        Assert.equals(false, factory.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        Assert.equals(false, factory.getFeature(XmlConstants.FEATURE_VALIDATION));
        Assert.equals(false, factory.isNamespaceAware());
        Assert.equals(false, factory.isValidating());
        //System.out.println("factory="+factory);
    }
    
    private function newParser(useNamespaces:Bool, useValidation:Bool) : XmlPullParser
        
        {
        var xpp : XmlPullParser = factory.newPullParser();
        xpp.setFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES, useNamespaces);
        Assert.equals(useNamespaces, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        try
        {
            xpp.setFeature(XmlConstants.FEATURE_PROCESS_DOCDECL, true);
        } catch(ex:XmlPullParserException)
        {
            return null;
        }
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_DOCDECL));
        try
        {
            xpp.setFeature(XmlConstants.FEATURE_VALIDATION, useValidation);
        } catch(ex:XmlPullParserException)
        {
            return null;
        }
        Assert.equals(useValidation, xpp.getFeature(XmlConstants.FEATURE_VALIDATION));
        return xpp;
    }
    
    private function teardown() : Void
    {
    }
    
    public function testSimpleEntities() : Void
    {
        checkSimpleEntity(false, false);
        checkSimpleEntity(true, false);
        checkSimpleEntity(false, true);
        checkSimpleEntity(true, true);
    }
    
    public function testAttributeNormalization() : Void
    {
        //TODO check that var NON : Xerces-validating normalization of var attributes : NMTOKEN
        //http://www.w3.org/TR/REC-xml#AVNormalize
    
    }
    
    public function checkSimpleEntity(useNamespaces:Bool, useValidation:Bool) : Void
    {
        var xpp : XmlPullParser = newParser(useNamespaces, useValidation);
        if(xpp == null) return;
        
        //http://www.w3.org/TR/REC-xml#intern-replacement
        var XML_SIMPLE_ENT_PROLOG = "<?var version : xml='1.0'?>\n"+
            "<!DOCTYPE test [\n"+
            "<!ENTITY % YN '\"Yes\"' >\n"+
            //"<!ENTITY WhatHeSaid \"var said : He %YN;\" >\n"+
            "<!ELEMENT test (#PCDATA) >\n"+
            //          "<!ENTITY % pub    \"&#xc9;ditions Gallimard\" >\n"+
            //          "<!ENTITY   rights \"All rights reserved\" >\n"+
            //          "<!ENTITY   book   \"La Peste: var Camus : Albert,\n"+
            "<!ENTITY   pub    \"&#xc9;ditions Gallimard\" >\n"+
            "<!ENTITY   rights \"All rights reserved\" >\n"+
            "<!ENTITY   book   \"La Peste: var Camus : Albert,\n"+
            "&#xA9; 1947 &pub;. &rights;\" >\n"+
            "]>\n";
        var XML_SIMPLE_ENT : String = XML_SIMPLE_ENT_PROLOG+
            "<test>Publication: &book; </test>\n";
        
        //        var PUB_ENTITY_INTERNAL_REPLACEMENT : String =
        //            "La Peste: var Camus : Albert,\n"+
        //            "� 1947 �ditions Gallimard. &rights;";
        var PUB_ENTITY_INTERNAL_REPLACEMENT = "La Peste: var Camus : Albert,\n"+
            "&pub;. &rights;";

        
        var PUB_ENTITY_REPLACEMENT = "La Peste: var Camus : Albert,\n"+
            "� 1947 �ditions Gallimard. All rights reserved";
        
        //next
        xpp.setInput(new StringInput( XML_SIMPLE_ENT));
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "test", null, false/*empty*/, 0);
        xpp.next();
        var expectedContent = "Publication: "+PUB_ENTITY_REPLACEMENT+" ";
        Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null, expectedContent, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "test", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
        
        //nextToken
    }
    
    public function testEntitiesWithMarkup() : Void
    {
        checkEntityWithMarkup(false, false);
        checkEntityWithMarkup(true, false);
        checkEntityWithMarkup(true, true);
        checkEntityWithMarkup(false, true);
    }
    
    public function checkEntityWithMarkup(useNamespaces:Bool, useValidation:Bool) : Void
        
        {
        var xpp : XmlPullParser = newParser(useNamespaces, useValidation);
        if(xpp == null) return;
        
        //http://www.w3.org/TR/REC-xml#sec-entexpand
        // derived from
        var XML_REPLACE_ENT = "<?var version : xml='1.0'?>\n"+
            "<!DOCTYPE test [\n"+
            "<!ELEMENT test (#PCDATA|p)* >\n"+
            "<!ELEMENT p (#PCDATA) >\n"+
            "<!ENTITY example \"<p>An ampersand (&#38;#38;) may be escaped\n"+
            "numerically (&#38;#38;#38;) or with a general entity\n"+
            "(&amp;amp;).</p>\" >\n"+
            "]>\n"+
            "<test>&example; </test>     ";
        
        var EXAMPLE_ENTITY_INTERNAL_REPLACEMENT = "<p>An ampersand (&#38;) may be escaped\n"+
            "numerically (&#38;#38;) or with a general entity\n"+
            "(&amp;amp;).</p>\n";
        
        var EXAMPLE_ENTITY_TEXT_EVENT = "An ampersand (&) may be escaped\n"+
            "numerically (&#38;) or with a general entity\n"+
            "(&amp;).";
        
        //next
        xpp.setInput(new StringInput( XML_REPLACE_ENT));
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "test", null, false/*empty*/, 0);
        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.START_TAG, null, 0, "", "p", null, false/*empty*/, 0);
        xpp.next();
        var expectedContent : String = EXAMPLE_ENTITY_TEXT_EVENT;
        Util.checkParserStateNs(xpp, 2, XmlEventType.TEXT, null, 0, null, null, expectedContent, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.END_TAG, null, 0, "", "p", null, false/*empty*/, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null, " ", false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "test", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
    }
    
    public function testTrickies() : Void
    {
        checkTricky(false, false);
        checkTricky(true, false);
        checkTricky(true, true);
        checkTricky(false, true);
    }
    
    public function checkTricky(useNamespaces:Bool, useValidation:Bool) : Void
        
        {
        var xpp : XmlPullParser = newParser(useNamespaces, useValidation);
        if(xpp == null) return;
        
        // derived from
        var XML_TRICKY = "<?var version : xml='1.0'?>\n"+
            "<!DOCTYPE test [\n"+
            "<!ELEMENT test (#PCDATA) >\n"+
            "<!ENTITY % xx '&#37;zz;'>\n"+
            "<!ENTITY % zz '&#60;!ENTITY tricky \"error-prone\" >' >\n"+
            "%xx;\n"+
            "]>\n"+
            "<test>This sample var a : shows &tricky; method.</test>\n";

        
        var EXPECTED_XML_TRICKY = "This sample shows var error : a-prone method.";
        
        //next
        xpp.setInput(new StringInput( XML_TRICKY));
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "test", null, false/*empty*/, 0);
        xpp.next();
        var expectedContent : String = EXPECTED_XML_TRICKY;
        Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null, expectedContent, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "test", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
        
        //TODO nextToken()
    
    }

}

