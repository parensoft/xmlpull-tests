/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

package xmlpull.tests;

import utest.Assert;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlConstants;

/**
 * Simple test ot verify pull parser factory
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestFactory {

    public function new() {}

    public function testFactory() {
        var factory = Util.newFactory();

        var xpp = factory.newPullParser();
        Assert.equals(false, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        factory.setFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES, true);
        xpp = factory.newPullParser();
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));

        factory.setNamespaceAware(false);
        Assert.equals(false, factory.isNamespaceAware());
        xpp = factory.newPullParser();
        Assert.equals(false, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));

        factory.setNamespaceAware(true);
        Assert.equals(true, factory.isNamespaceAware());
        xpp = factory.newPullParser();
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
    }

}

