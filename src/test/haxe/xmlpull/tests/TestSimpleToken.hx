/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

package xmlpull.tests;

import haxe.io.StringInput;

import utest.Assert;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;
import xmlpull.XmlConstants;

/**
 * Simple test for minimal XML tokenizing
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestSimpleToken {
    var factory: XmlPullParserFactory;

    public function new() {}

    public function setup() {
        factory = Util.newFactory();

        factory.setNamespaceAware(true);
        Assert.equals(true, factory.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
    }

    public function testVerySimpleToken() {
      var xpp = factory.newPullParser();
      Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
      xpp.setInput(new StringInput("<foo><!--comment--><?target pi?></foo>"));
      Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
      xpp.nextToken();
      Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "foo", null, false, 0);
      xpp.nextToken();
      Util.checkParserStateNs(xpp, 1, XmlEventType.COMMENT, null, 0, null, null, "comment", false, -1);
      xpp.nextToken();
      Util.checkParserStateNs(xpp, 1, XmlEventType.PROCESSING_INSTRUCTION, null, 0, null, null, "target pi", false, -1);
      xpp.nextToken();
      Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "foo", null, false, -1);
      xpp.nextToken();
      Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
    }

    public function testSimpleToken() {
      var xpp: XmlPullParser = factory.newPullParser();
      Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));

      // check setInput semantics
      Assert.equals(XmlEventType.START_DOCUMENT, xpp.getEventType());
      try {
        xpp.nextToken();
        Assert.fail("exception was expected of nextToken() if no input was set on parser");
        } catch(ex: XmlPullParserException) {}

        xpp.setInput(null);
        Assert.equals(XmlEventType.START_DOCUMENT, xpp.getEventType());
        try {
            xpp.nextToken();
            Assert.fail("exception was expected of next() if no input was set on parser");
        } catch(ex: XmlPullParserException) {}

        xpp.setInput(null); //reset parser
        var FEATURE_XML_ROUNDTRIP="http://xmlpull.org/v1/doc/features.html#xml-roundtrip";
        // attempt to set roundtrip
        try {
            xpp.setFeature(FEATURE_XML_ROUNDTRIP, true);
        } catch(ex: Dynamic) {
        }
        // did we succeeded?
        var roundtripSupported = xpp.getFeature(FEATURE_XML_ROUNDTRIP);


        // check the simplest possible XML document - just one root element
        for(i in 1...3) {
            xpp.setInput(new StringInput(i == 1 ? "<foo/>" : "<foo></foo>"));
            var empty = (i == 1);
            Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
            xpp.nextToken();
            Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "foo", null, empty, 0);
            if(roundtripSupported) {
                if(empty) {
                    //              System.out.println("tag='"+xpp.getText()+"'");
                    //              String foo ="<foo/>";
                    //              String foo2 = xpp.getText();
                    //              System.out.println(foo.equals(foo2));
                    Assert.equals("empty tag roundtrip",
                                 Util.printable("<foo/>"),
                                 Util.printable(xpp.getText()));
                } else {
                    Assert.equals("start tag roundtrip",
                                 Util.printable("<foo>"),
                                 Util.printable(xpp.getText()));
                }
            }
            xpp.nextToken();
            Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "foo", null, false, -1);
            if(roundtripSupported) {
                if(empty) {
                    Assert.equals("empty tag roundtrip",
                                 Util.printable("<foo/>"),
                                 Util.printable(xpp.getText()));
                } else {
                    Assert.equals("end tag roundtrip",
                                 Util.printable("</foo>"),
                                 Util.printable(xpp.getText()));
                }
            }
            xpp.nextToken();
            Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
        }
    }

    public function testNormalizations() {
      var xpp = factory.newPullParser();
      Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
      checkNormalization(xpp);
      try{ xpp.setFeature(XmlConstants.FEATURE_XML_ROUNDTRIP, false); } catch(ex: Dynamic) {}
      checkNormalization(xpp);
      try{ xpp.setFeature(XmlConstants.FEATURE_XML_ROUNDTRIP, true); } catch(ex: Dynamic) {}
      checkNormalization(xpp);
    }

    public function checkNormalization(xpp: XmlPullParser) {
      xpp.setInput(new StringInput("<foo>\n \r\n \n\r</foo>"));
      Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
      xpp.nextToken();
      Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "foo", null, false/*empty*/, 0);

      var roundtrip = xpp.getFeature(XmlConstants.FEATURE_XML_ROUNDTRIP) == false;
      var text = Util.nextTokenGathered(xpp, XmlEventType.TEXT, true);
      if(roundtrip) {
        Assert.equals(Util.printable("\n \n \n\n"), Util.printable(text));
        Assert.equals("\n \n \n\n", text);
      } else {
        Assert.equals(Util.printable("\n \r\n \n\r"), Util.printable(text));
        Assert.equals("\n \r\n \n\r", text);
      }

      Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "foo", null, false, -1);
      xpp.nextToken();
      Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
    }

}

