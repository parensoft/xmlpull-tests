/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

/* -*-             c-basic-offset: 4; indent-tabs-mode: nil; -*-  //------100-columns-wide------>|*/
// for license see accompanying LICENSE_TESTS.txt file (available also at http://www.xmlpull.org)

package xmlpull.tests;

import utest.Assert;

import haxe.io.Bytes;

import haxe.io.StringInput;
import haxe.io.BytesOutput;



import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;

import xmlpull.XmlConstants.*;

/**
 * Conformance test to verify nextToken() behavior.
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestToken 
{
    private var factory : XmlPullParserFactory;
    
    public function new()
    {
        
    }
    
    private function setup() : Void
    {
        factory = Util.newFactory();
        factory.setFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES, true);
        Assert.equals(true, factory.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        Assert.equals(false, factory.getFeature(XmlConstants.FEATURE_VALIDATION));
    }
    
    private function teardown() : Void
    {
        factory = null;
    }

    
    inline private static var FOO_XML = "<!DOCTYPE titlepage "+
        "[<!ENTITY % active.links \"INCLUDE\">"+
        "  <!ENTITY   test \"This is test! Do NOT Panic!\" >"+
        "]>"+
        "<foo attrName='attrVal'>bar<!--comment\r\ntest-->"+
        "&test;&test;&lt;&#32;"+
        "&amp;&gt;&apos;&quot;&#x20;&#x3C;"+
        "<?pi ds\r\nda?><![CDATA[ vo<o ]]></foo>";
    inline private static var MISC_XML : String =
        //"\n \r\n \n\r<!DOCTYPE titlepage SYSTEM \"http://www.foo.bar/dtds/typo.dtd\""+
        //"<!--c2-->"+
        //"<?xml version='1.0'?><!--c-->"+
        //"   \r\n <?c x?>"+
        "\n \r\n \n\r"+
        "<!--c-->  \r\n"+
        FOO_XML+
        " \r\n";


    
    public function testTokenEventEquivalency() : Void
    {
        var xpp : XmlPullParser = factory.newPullParser();
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        xpp.setInput(new StringInput(MISC_XML));
        var processDocdecl : Bool = xpp.getFeature(XmlConstants.FEATURE_PROCESS_DOCDECL);
        
        // make sure entity "test" can be resolved even when parser is not parsing DOCDECL
        if(!processDocdecl)
        {
            xpp.defineEntityReplacementText("test", "This is test! Do NOT Panic!");
        }
        
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "foo", null, false, 1);
        Util.checkAttribNs(xpp, 0, null, "", "attrName", "attrVal");
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null,
                           "barThis is test! Do NOT Panic!This is test! Do NOT Panic!< &>'\" < vo<o ",
                           false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "foo", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
    
    }
    
    public function testTokenTypes() : Void
    {
        checkTokenTypes2(false, false);
        checkTokenTypes2(false, true);
    }
    
    public function testTokenTypesInPrologAndEpilog() : Void
    {
        checkTokenTypes2(true, false);
        checkTokenTypes2(true, false);
    }
    
    // one step further - it has content ...
    public function checkTokenTypes2(checkPrologAndEpilog:Bool,
                               useRoundtrip:Bool) : Void
                               {
        var xpp : XmlPullParser = factory.newPullParser();
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        //System.out.println("using "+xpp);
        
        if(checkPrologAndEpilog)
        {
            xpp.setInput(new StringInput(MISC_XML));
        } else
        {
            xpp.setInput(new StringInput(FOO_XML));
        }
        
        // attempt to set roundtrip
        try
        {
            xpp.setFeature(XmlConstants.FEATURE_XML_ROUNDTRIP, useRoundtrip);
        } catch (ex:Dynamic) {  // make sure we ignore if failed to set roundtrip ...
        }
        // did we succeeded?
        var roundtripSupported : Bool = xpp.getFeature(XmlConstants.FEATURE_XML_ROUNDTRIP);
        //boolean unnormalizedSupported = xpp.getFeature(FEATURE_UNNORMALIZED_XML);
        if(!useRoundtrip && roundtripSupported != false)
        {
            throw new String(
                "disabling feature "+XmlConstants.FEATURE_XML_ROUNDTRIP+" must be supported");
        }
        
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        try
        {
            xpp.isWhitespace();
            Assert.fail("whitespace function must fail for START_DOCUMENT");
        } catch(ex:XmlPullParserException)
        {
        }
        
        if(checkPrologAndEpilog)
        {
            xpp.nextToken();
            if(xpp.getEventType() == XmlEventType.IGNORABLE_WHITESPACE)
            {
                //              xpp.nextToken();
                //              checkParserStateNs(xpp, 0, xpp.IGNORABLE_WHITESPACE, null, 0, null, null,
                //                                 "\n \r\n \n\r", false, -1);
                //              assertTrue(xpp.isWhitespace());
                var text : String = Util.gatherTokenText(xpp, XmlEventType.IGNORABLE_WHITESPACE, true);
                if(roundtripSupported)
                {
                    Assert.equals(Util.printable("\n \r\n \n\r"), Util.printable(text));
                    Assert.equals("\n \r\n \n\r", text);
                } else
                {
                    Assert.equals(Util.printable("\n \n \n\n"), Util.printable(text));
                    Assert.equals("\n \n \n\n", text);
                }
            }
            
            Util.checkParserStateNs(xpp, 0, XmlEventType.COMMENT, null, 0, null, null, "c", false, -1);
            try
            {
                xpp.isWhitespace();
                Assert.fail("whitespace function must fail for START_DOCUMENT");
            } catch(ex:XmlPullParserException)
            {
            }
            
            xpp.nextToken();
            if(xpp.getEventType() == XmlEventType.IGNORABLE_WHITESPACE)
            {
                //              xpp.nextToken();
                //              checkParserStateNs(xpp, 0, xpp.IGNORABLE_WHITESPACE, null, 0, null, null, "  \r\n", false, -1);
                //              assertTrue(xpp.isWhitespace());
                //String text = nextTokenGathered(xpp, xpp.IGNORABLE_WHITESPACE, true);
                var text : String = Util.gatherTokenText(xpp, XmlEventType.IGNORABLE_WHITESPACE, true);
                if(roundtripSupported)
                {
                    Assert.equals(Util.printable("  \r\n"), Util.printable(text));
                } else
                {
                    Assert.equals(Util.printable("  \n"), Util.printable(text));
                }
            }

        
        } else
        {
            xpp.nextToken();
        }
        Util.checkParserStateNs9(xpp, 0, XmlEventType.DOCDECL, null, 0, null, null, false, -1);
        var expectedDocdecl = " titlepage "+
            //"SYSTEM \"http://www.foo.bar/dtds/typo.dtd\""+
            "[<!ENTITY % active.links \"INCLUDE\">"+
            "  <!ENTITY   test \"This is test! Do NOT Panic!\" >]";
        var gotDocdecl : String = xpp.getText();
        if(roundtripSupported && gotDocdecl == null)
        {
            Assert.fail("when roundtrip is enabled DOCDECL content must be reported");
        }
        if(gotDocdecl != null)
        {
            Assert.equals(expectedDocdecl, gotDocdecl);
        }
        
        try
        {
            xpp.isWhitespace();
            Assert.fail("whitespace function must fail for DOCDECL");
        } catch(ex:XmlPullParserException)
        {
        }
        
        // now parse elements
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "foo", null, false, 1);
        if(roundtripSupported)
        {
            Assert.equals("<foo attrName='attrVal'>", xpp.getText());
        }
        Util.checkAttribNs(xpp, 0, null, "", "attrName", "attrVal");
        try
        {
            xpp.isWhitespace();
            Assert.fail("whitespace function must fail for START_DOCUMENT");
        } catch(ex:XmlPullParserException)
        {
        }
        
        {
            var text : String = Util.nextTokenGathered(xpp, XmlEventType.TEXT, false);
            Assert.equals(Util.printable("bar"), Util.printable(text));
        }
        
        //xpp.nextToken();
        
        Util.checkParserStateNs9(xpp, 1, XmlEventType.COMMENT, null, 0, null, null, false, -1);
        try
        {
            xpp.isWhitespace();
            Assert.fail("whitespace function must fail for COMMENT");
        } catch(ex:XmlPullParserException)
        {
        }
        {
            var text : String = xpp.getText();
                if(roundtripSupported)
                {
                    Assert.equals(Util.printable("comment\r\ntest"), Util.printable(text));
                } else
                {
                    Assert.equals(Util.printable("comment\ntest"), Util.printable(text));
                }
            }
        
        var processDocdecl : Bool = xpp.getFeature(XmlConstants.FEATURE_PROCESS_DOCDECL);
        
        // uresolved entity must be reurned as null by nextToken()
        xpp.nextToken();
        if(!processDocdecl)
        {
            Util.checkParserStateNs(xpp, 1, XmlEventType.ENTITY_REF, null, 0, null,
                               "test", null, false, -1);
        } else
        {
            Util.checkParserStateNs(xpp, 1, XmlEventType.ENTITY_REF, null, 0, null,
                               "test", "This is test! Do NOT Panic!", false, -1);
        
        }
        
        // now we check if we can resolve entity
        if(!processDocdecl)
        {
            xpp.defineEntityReplacementText("test", "This is test! Do NOT Panic!");
        }
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 1, XmlEventType.ENTITY_REF, null, 0, null,
                           "test", "This is test! Do NOT Panic!", false, -1);
        try
        {
            xpp.isWhitespace();
            Assert.fail("whitespace function must fail for ENTITY_RED");
        } catch(ex:XmlPullParserException)
        {
        }
        
        // check standard entities and char refs
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 1, XmlEventType.ENTITY_REF, null, 0, null, "lt", "<", false, -1);
        try
        {
            xpp.isWhitespace();
            Assert.fail("whitespace function must fail for ENTITY_REF");
        } catch(ex:XmlPullParserException)
        {
        }
        
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 1, XmlEventType.ENTITY_REF, null, 0, null, "#32", " ", false, -1);
        try
        {
            xpp.isWhitespace();
            Assert.fail("whitespace function must fail for ENTITY_REF");
        } catch(ex:XmlPullParserException)
        {
        }
        
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 1, XmlEventType.ENTITY_REF, null, 0, null, "amp", "&", false, -1);
        
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 1, XmlEventType.ENTITY_REF, null, 0, null, "gt", ">", false, -1);
        
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 1, XmlEventType.ENTITY_REF, null, 0, null, "apos", "'", false, -1);
        
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 1, XmlEventType.ENTITY_REF, null, 0, null, "quot", "\"", false, -1);
        
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 1, XmlEventType.ENTITY_REF, null, 0, null, "#x20", " ", false, -1);
        
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 1, XmlEventType.ENTITY_REF, null, 0, null, "#x3C", "<", false, -1);
        
        xpp.nextToken();
        Util.checkParserStateNs9(xpp, 1, XmlEventType.PROCESSING_INSTRUCTION, null, 0, null, null, false, -1);
        try
        {
            xpp.isWhitespace();
            Assert.fail("whitespace function must fail for START_DOCUMENT");
        } catch(ex:XmlPullParserException)
        {
        }
        {
            var text : String = xpp.getText();
                if(roundtripSupported)
                {
                    Assert.equals(Util.printable("pi ds\r\nda"), Util.printable(text));
                } else
                {
                    Assert.equals(Util.printable("pi ds\nda"), Util.printable(text));
                }
            }
        
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 1, XmlEventType.CDSECT, null, 0, null, null, " vo<o ", false, -1);
        Assert.equals(false, xpp.isWhitespace());
        
        xpp.nextToken();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "foo", null, false, -1);
        if(roundtripSupported)
        {
            Assert.equals("</foo>", xpp.getText());
        }
        try
        {
            xpp.isWhitespace();
            Assert.fail("whitespace function must fail for END_TAG");
        } catch(ex:XmlPullParserException)
        {
        }
        
        xpp.nextToken();
        if(checkPrologAndEpilog)
        {
            //if(unnormalizedSupported) {
            if(xpp.getEventType() == XmlEventType.IGNORABLE_WHITESPACE)
            {
                
                //              xpp.nextToken();
                //              checkParserStateNs(xpp, 0, xpp.IGNORABLE_WHITESPACE, null, 0, null, null,
                //                                 " \r\n", false, -1);
                //              assertTrue(xpp.isWhitespace());
                //String text = nextTokenGathered(xpp, xpp.IGNORABLE_WHITESPACE, true);
                var text : String = Util.gatherTokenText(xpp, XmlEventType.IGNORABLE_WHITESPACE, true);
                if(roundtripSupported)
                {
                    Assert.equals(Util.printable(" \r\n"), Util.printable(text));
                } else
                {
                    Assert.equals(Util.printable(" \n"), Util.printable(text));
                }
            }
        }
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
        try
        {
            xpp.isWhitespace();
            Assert.fail("whitespace function must fail for END_DOCUMENT");
        } catch(ex:XmlPullParserException)
        {
        }
    
    }
    
    // one step further - it has content ...
    public function testXmlRoundtrip() : Void
    {
        var xpp : XmlPullParser = factory.newPullParser();
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));

        
        // reset parser
        xpp.setInput(null);
        // attempt to set roundtrip
        try
        {
            xpp.setFeature(XmlConstants.FEATURE_XML_ROUNDTRIP, true);
        } catch (ex:Dynamic)
        {
        }
        // did we succeeded?
        var roundtripSupported : Bool = xpp.getFeature(XmlConstants.FEATURE_XML_ROUNDTRIP);
        
        if(!roundtripSupported)
        {
            return;
        }
        
        var sw : BytesOutput = new BytesOutput();
        //StringWriter st = new StringWriter();
        xpp.setInput(new StringInput(MISC_XML));
        while(xpp.nextToken() != XmlEventType.END_DOCUMENT)
        {
            var ev = xpp.getEventType();
            var s = xpp.getText();
            switch(ev) {
                case XmlEventType.START_TAG:
                    s = xpp.getText();
                    Assert.equals(xpp.getText(), s);
                    sw.write(Bytes.ofString(s));
                    break;
                case XmlEventType.END_TAG:
                    s = xpp.getText();
                    Assert.equals(xpp.getText(), s);
                    sw.write(Bytes.ofString(s));
                    break;
                case XmlEventType.TEXT:
                    s = xpp.getText();
                    Assert.equals(xpp.getText(), s);
                    sw.write(Bytes.ofString(s));
                    break;
                case XmlEventType.IGNORABLE_WHITESPACE:
                    s = xpp.getText();
                    Assert.equals(xpp.getText(), s);
                    sw.write(Bytes.ofString(s));
                    break;
                case XmlEventType.CDSECT:
                    s = xpp.getText();
                    sw.write(Bytes.ofString("<![CDATA["));
                    Assert.equals(xpp.getText(), s);
                    sw.write(Bytes.ofString(s));
                    sw.write(Bytes.ofString("]]>"));
                    break;
                case XmlEventType.PROCESSING_INSTRUCTION:
                    s = xpp.getText();
                    sw.write(Bytes.ofString("<?"));
                    Assert.equals(xpp.getText(), s);
                    sw.write(Bytes.ofString(s));
                    sw.write(Bytes.ofString("?>"));
                    break;
                case XmlEventType.COMMENT:
                    s = xpp.getText();
                    sw.write(Bytes.ofString("<!--"));
                    Assert.equals(xpp.getText(), s);
                    sw.write(Bytes.ofString(s));
                    sw.write(Bytes.ofString("-->"));
                    break;
                case XmlEventType.ENTITY_REF:
                    s = xpp.getText();
                    sw.write(Bytes.ofString("&"));
                    Assert.equals(xpp.getName(), s);
                    sw.write(Bytes.ofString(s));
                    sw.write(Bytes.ofString(";"));
                    break;
                case XmlEventType.DOCDECL:
                    s = xpp.getText();
                    sw.write(Bytes.ofString("<!DOCTYPE"));
                    Assert.equals(xpp.getText(), s);
                    sw.write(Bytes.ofString(s));
                    sw.write(Bytes.ofString(">"));
                    break;
                default:
                    throw new String("unknown token type");
            }
        }
        sw.close();
        var RESULT_XML_BUF : String = sw.getBytes().toString();
        Assert.equals(Util.printable(MISC_XML), Util.printable(RESULT_XML_BUF));
    }


}

