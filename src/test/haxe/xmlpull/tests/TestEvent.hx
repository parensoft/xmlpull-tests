/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

package xmlpull.tests;

import utest.Assert;

import haxe.io.StringInput;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;
import xmlpull.XmlConstants;

/**
 * More complete test to verify paring.
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestEvent {
    var factory: XmlPullParserFactory;

    public function new() {}

    public function setup() {
        factory = Util.newFactory();

        factory.setNamespaceAware(true);
        Assert.equals(true, factory.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        Assert.equals(false, factory.getFeature(XmlConstants.FEATURE_VALIDATION));
    }

    public function testEvent() {
        var xpp = factory.newPullParser();
        xpp.setInput(new StringInput(Util.TEST_XML));

        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);

        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "root", null, false, 0);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null, "\n", false, -1);

        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.START_TAG, null, 0, "", "foo", null, false, 0);
        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.TEXT, null, 0, null, null, "bar", false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.END_TAG, null, 0, "", "foo", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null, "\n", false, -1);

        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.START_TAG,
                           null, 0, "http://www.xmlpull.org/temp", "hugo", null, false, 0);

        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.TEXT, null, 0, null, null, " \n\n \n  ", false, -1);

        xpp.next();
        Util.checkParserStateNs(xpp, 3, XmlEventType.START_TAG,
                           null, 0, "http://www.xmlpull.org/temp", "hugochild", null, false, 0);
        xpp.next();
        Util.checkParserStateNs(xpp, 3, XmlEventType.TEXT, null, 0, null, null,
                           "This is in a new namespace", false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 3, XmlEventType.END_TAG,
                           null, 0, "http://www.xmlpull.org/temp", "hugochild", null, false, -1);

        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.END_TAG,
                           null, 0, "http://www.xmlpull.org/temp", "hugo", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null, "\t\n", false, -1);

        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.START_TAG, null, 0, "", "bar", null, true, 1);
        Util.checkAttribNs(xpp, 0, null, "", "testattr", "123abc");
        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.END_TAG, null, 0, "", "bar", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "root", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
    }

    public function testMultiNs(): Void {
        var xpp = factory.newPullParser();
        xpp.setInput(new StringInput("<foo><bar xmlns=''/><char xmlns=''></char></foo>"));

        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);

        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "foo", null, false, 0);
        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.START_TAG, null, 0, "", "bar", null, true, 0);
        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.END_TAG, null, 0, "", "bar", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.START_TAG, null, 0, "", "char", null, false, 0);
        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.END_TAG, null, 0, "", "char", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "foo", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);

        xpp.setInput(new StringInput("<foo><bar xmlns=''></bar><char xmlns=''></char></foo>"));

        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "foo", null, false, 0);
        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.START_TAG, null, 0, "", "bar", null, false, 0);
        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.END_TAG, null, 0, "", "bar", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.START_TAG, null, 0, "", "char", null, false, 0);
        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.END_TAG, null, 0, "", "char", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "foo", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);

     }

}

