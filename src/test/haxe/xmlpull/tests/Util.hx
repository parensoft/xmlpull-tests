/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

package xmlpull.tests;

import haxe.ds.Vector;

import utest.Assert;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;
import xmlpull.XmlConstants;
import xmlpull.XmlEventType;

/**
 * Some common utilities to help with XMLPULL tests.
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class Util {

    public static inline var TEST_XML =
        "<root>\n"+
        "<foo>bar</foo>\r\n"+
        "<hugo xmlns=\"http://www.xmlpull.org/temp\"> \n\r \n"+
        "  <hugochild>This is in a <!-- comment -->new namespace</hugochild>"+
        "</hugo>\t\n"+
        "<bar testattr='123abc' />"+
        "</root>\n"+
        "\n"+
        "<!-- an xml sample document without meaningful content -->\n";


    public static function checkParserState(
        xpp: XmlPullParser,
        depth: Int,
        type: Int,
        name: String,
        text: String,
        isEmpty: Bool,
        attribCount: Int
    ) {
        Assert.equals(false, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
        //  Assert.equals(xpp.TYPES[type], xpp.TYPES[xpp.getEventType()], "TYPES[getType()]");
        Assert.equals(type, xpp.getEventType(), "getType()");
        Assert.equals(depth, xpp.getDepth());
        Assert.equals(null, xpp.getPrefix(), "getPrefix()");
        Assert.equals(0, xpp.getNamespaceCount(depth), "getNamespacesCount(getDepth())");
        if(xpp.getEventType() == XmlEventType.START_TAG || xpp.getEventType() == XmlEventType.END_TAG) {
            Assert.equals("", xpp.getNamespace(), "getNamespace()");
        } else {
            Assert.equals(null, xpp.getNamespace(), "getNamespace()");
        }
        Assert.equals(name, xpp.getName(), "getName()");

        if(xpp.getEventType() != XmlEventType.START_TAG && xpp.getEventType() != XmlEventType.END_TAG) {
            Assert.equals(printable(text), printable(xpp.getText()), "getText()");

        }

        if(type == XmlEventType.START_TAG) {
            Assert.equals(isEmpty, xpp.isEmptyElementTag(), "isEmptyElementTag()");
        } else {
            try {
                xpp.isEmptyElementTag();
                Assert.fail("isEmptyElementTag() must throw exception if parser not on START_TAG");
            } catch(ex: XmlPullParserException) {
            }
        }
        Assert.equals(attribCount, xpp.getAttributeCount());
    }

    public static function checkParserStateNs8(
        xpp:XmlPullParser,
        depth:Int,
        type:Int,
        nsCount:Int,
        namespace:String,
        name:String,
        isEmpty:Bool,
        attribCount:Int
        ) : Void
    {
      Assert.isTrue( xpp.getLineNumber() == -1 || xpp.getLineNumber() >= 1);
      Assert.isTrue( xpp.getColumnNumber() == -1 || xpp.getColumnNumber() >= 0);

      // this methid can be used with enabled and not var namespaces : enabled
      //Assert.equals("PROCESS_NAMESPACES", true, xpp.getFeature(xpp.FEATURE_PROCESS_NAMESPACES));
      Assert.equals(type, xpp.getEventType());
        Assert.equals(name, xpp.getName());
        
        Assert.equals(depth, xpp.getDepth());
        Assert.equals(nsCount, xpp.getNamespaceCount(depth));
        Assert.equals(namespace, xpp.getNamespace());
        
        if(type == XmlEventType.START_TAG)
        {
            Assert.equals(isEmpty, xpp.isEmptyElementTag());
        } else
        {
            try {
                xpp.isEmptyElementTag();
                Assert.fail("isEmptyElementTag() must throw exception if parser not on START_TAG");
            } catch(ex:XmlPullParserException)
            {
            }
        }
        Assert.equals(attribCount, xpp.getAttributeCount());
    }

    public static function checkParserStateNs9(
        xpp:XmlPullParser,
        depth:Int,
        type:Int,
        prefix:String,
        nsCount:Int,
        namespace:String,
        name:String,
        isEmpty:Bool,
        attribCount:Int
) : Void
    {
        checkParserStateNs8(xpp, depth, type, nsCount, namespace, name, isEmpty, attribCount);
        Assert.equals(prefix, xpp.getPrefix());
    }
 
 
    public static function checkParserStateNs(
        xpp:XmlPullParser,
        depth:Int,
        type:Int,
        prefix:String,
        nsCount:Int,
        namespace:String,
        name:String,
        text:String,
        isEmpty:Bool,
        attribCount:Int
) : Void
    {
        checkParserStateNs9(xpp, depth, type, prefix, nsCount, namespace, name, isEmpty, attribCount);
        
        if(xpp.getEventType() != XmlEventType.START_TAG && xpp.getEventType() != XmlEventType.END_TAG)
        {
            Assert.equals(printable(text), printable(xpp.getText()));
            
        }

    
    }

    public static function checkParserStateNsOld(
        xpp: XmlPullParser,
        depth: Int,
        type: Int,
        prefix: String,
        nsCount: Int,
        namespace: String,
        name: String,
        text: String,
        isEmpty: Bool,
        attribCount: Int
    ) {
        // this methid can be used with enabled and not enabled namespaces
        //Assert.equals(true, xpp.getFeature(xpp.FEATURE_PROCESS_NAMESPACES), "PROCESS_NAMESPACES");
        Assert.equals(type, xpp.getEventType(), "getType()");
        Assert.equals(name, xpp.getName(), "getName()");

        Assert.equals(depth, xpp.getDepth());
        Assert.equals(prefix, xpp.getPrefix(), "getPrefix()");
        Assert.equals(nsCount, xpp.getNamespaceCount(depth));
        Assert.equals(namespace, xpp.getNamespace());

        /*
         * no checkTextCharacters()
         *
        if(xpp.getEventType() != XmlEventType.START_TAG && xpp.getEventType() != XmlEventType.END_TAG) {
            Assert.equals(printable(text), printable(xpp.getText()), "getText()");

            int [] holderForStartAndLength = new int[2];
            char[] buf = xpp.getTextCharacters(holderForStartAndLength);
            if(buf != null) {
                String s = new String(buf, holderForStartAndLength[0], holderForStartAndLength[1]);
                Assert.equals(printable(text), printable(s), "getText(holder)");
            } else {
                Assert.equals(null, text, "getTextCharacters()");
            }

        }
        */

        if(type == XmlEventType.START_TAG) {
            Assert.equals(isEmpty, xpp.isEmptyElementTag(), "isEmptyElementTag()");
        } else {
            try {
                xpp.isEmptyElementTag();
                Assert.fail("isEmptyElementTag() must throw exception if parser not on START_TAG");
            } catch(ex: XmlPullParserException) {
            }
        }
        Assert.equals(attribCount, xpp.getAttributeCount());
    }

    public static function checkAttrib(
        xpp: XmlPullParser,
        pos: Int,
        name: String,
        value: String
    ) {
        Assert.equals(XmlEventType.START_TAG, xpp.getEventType(), "must be on START_TAG");
        Assert.equals(null, xpp.getAttributePrefix(pos), "getAttributePrefix()");
        Assert.equals("", xpp.getAttributeNamespace(pos), "getAttributeNamespace()");
        Assert.equals(name, xpp.getAttributeName(pos), "getAttributeName()");
        Assert.equals(value, xpp.getAttributeValueByIndex(pos), "getAttributeValue()");
        Assert.equals(value, xpp.getAttributeValue(null, name), "getAttributeValue(name)");

    }


    public static function checkAttribNs5(
        xpp:XmlPullParser,
        pos:Int,
        namespace:String,
        name:String,
        value:String
        ) : Void
    {
      Assert.equals(XmlEventType.START_TAG, xpp.getEventType());
      Assert.equals(namespace, xpp.getAttributeNamespace(pos));
      Assert.equals(name, xpp.getAttributeName(pos));
      Assert.equals(Util.printable(value), Util.printable(xpp.getAttributeValueByIndex(pos)));
      Assert.equals( Util.printable(value), Util.printable(xpp.getAttributeValue(namespace, name)));
      Assert.equals("CDATA", xpp.getAttributeType(pos));
      Assert.equals(false, xpp.isAttributeDefault(pos));
    }

    public static function checkAttribNs(
        xpp:XmlPullParser,
        pos:Int,
        prefix:String,
        namespace:String,
        name:String,
        value:String
) : Void
    {
        checkAttribNs5(xpp, pos, namespace, name, value);
        Assert.equals(prefix, xpp.getAttributePrefix(pos));
    }
 
    public static function checkAttribNsOld(
        xpp: XmlPullParser,
        pos: Int,
        prefix: String,
        namespace: String,
        name: String,
        value: String
    ) {
        Assert.equals(XmlEventType.START_TAG, xpp.getEventType(), "must be on START_TAG");
        Assert.equals(prefix, xpp.getAttributePrefix(pos), "getAttributePrefix()");
        Assert.equals(namespace, xpp.getAttributeNamespace(pos), "getAttributeNamespace()");
        Assert.equals(name, xpp.getAttributeName(pos), "getAttributeName()");
        Assert.equals(printable(value), printable(xpp.getAttributeValueByIndex(pos)), "getAttributeValue()");
        Assert.equals(printable(value), printable(xpp.getAttributeValue(namespace, name)),
                     "getAttributeValue(ns,name)");
    }

    public static function checkNamespace(
        xpp: XmlPullParser,
        pos: Int,
        prefix: String,
        uri: String,
        checkMapping: Bool
    ) 
    {
        Assert.equals(prefix, xpp.getNamespacePrefix(pos), "getNamespacePrefix()");
        Assert.equals(uri, xpp.getNamespaceUri(pos), "getNamespaceUri()");
        if(checkMapping) {
            Assert.equals(uri, xpp.getNamespaceByPrefix (prefix), "getNamespace(prefix)");
        }
    }

    public static function printableChar(ch: Int) {
        if(ch == "\n".code) {
            return "\\n";
        } else if(ch == "\r".code) {
            return "\\r";
        } else if(ch == "\t".code) {
            return "\\t";
        } if(ch > 127 || ch < 32) {
            return "\\u"+toHex(ch);
        }
        return String.fromCharCode(ch);
    }

    public static function printable(s: String) {
        if(s == null) return null;
        var buf = new StringBuf();
        for(i in 0...s.length) {
            buf.add(printableChar(s.charCodeAt(i)));
        }
        s = buf.toString();
        return s;
    }

    private inline static function toHex(x:Int):String {
      /*
       *  stolen from polygonal-printf
       */
#if flash9
      var n:UInt = x;
      var s:String = untyped x.toString(16);
      s = s.toUpperCase();
#else
      var s = "";
      var hexChars = "0123456789ABCDEF";
      do
      {
        s = hexChars.charAt(x&15) + s;
        x >>>= 4;
      }
      while( x > 0 );
#end
      return s;
    }

    /**
     *
     * Mpve to next token and gather getText() for successive tokens of the same type.
     * Parser will be positioned on next token of different type.
     */
    public static function nextTokenGathered(xpp: XmlPullParser, type: XmlEventType, expectedWhitespaces: Bool) {
      xpp.nextToken();
      return gatherTokenText(xpp, type, expectedWhitespaces);
    }

    /**
     * Gathers getText() for successive tokens of the same type.
     * Parser will be positioned on next token of different type.
     */
    public static function gatherTokenText(xpp: XmlPullParser, type: XmlEventType, expectedWhitespaces: Bool) {
      var buf = new StringBuf();
      Assert.equals(type, xpp.getEventType());

      do {
        buf.add(xpp.getText());
        if(expectedWhitespaces) {
            Assert.isTrue(xpp.isWhitespace());
          }
        } while(xpp.nextToken() == type);

      return buf.toString();

    }

    public static inline var FACTORY_NAME_ENV = "XMLPULL_FACTORY";

    public static function newFactory() : XmlPullParserFactory {
#if sys
      var clname = Sys.getEnv(FACTORY_NAME_ENV);
      var cls = Type.resolveClass(clname);

      if (cls == null) throw 'class not found: $clname';

      return Type.createInstance(cls, []);
#else
      throw "not a system platform";
#end
    }

}

