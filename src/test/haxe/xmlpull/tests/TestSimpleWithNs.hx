/****
* XMLPULL API TESTS LICENSE
* --------------------------------------
* 
* XMLPULL V1 API TESTS
* Copyright (C) 2002 Aleksander Slominski
* For the Haxe port Copyright (c) 2015 Parensoft.NET
* 
* XMLPULL V1 API TESTS are free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* XMLPULL V1 API TESTS are distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* (see below and at http://www.gnu.org/copyleft/lesser.html).
* 
* 
* NOTE: XMLPULL V1 API TESTS are released under the Lesser GPL (LGPL) license, 
* granting you permission to use them in commercial and non-commercial applications for
* free. Unlike regular GPL, LGPL does not force you to license your own software under GPL. 
* 
* 
****/

/* -*-             c-basic-offset: 4; indent-tabs-mode: nil; -*-  //------100-columns-wide------>|*/
// see LICENSE_TESTS.txt in distribution for copyright and license information

package xmlpull.tests;

import haxe.io.StringInput;

import utest.Assert;

import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserFactory;
import xmlpull.XmlPullParserException;
import xmlpull.XmlConstants;

/**
 * Simple test for minimal XML parsing with namespaces
 *
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 */
class TestSimpleWithNs {
  var factory: XmlPullParserFactory;

  public function new() {}


  public function setup() {
    factory = Util.newFactory();

    factory.setNamespaceAware(true);
    Assert.equals(true, factory.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));
  }

    public function testSimpleWithNs() {
        var xpp: XmlPullParser = factory.newPullParser();
        Assert.equals(true, xpp.getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES));

        // check setInput semantics
        Assert.equals(XmlEventType.START_DOCUMENT, xpp.getEventType());
        try {
            xpp.next();
            Assert.fail("exception was expected of next() if no input was set on parser");
        } catch(ex: XmlPullParserException) {}

        xpp.setInput(null);
        Assert.equals(XmlEventType.START_DOCUMENT, xpp.getEventType());
        try {
            xpp.next();
            Assert.fail("exception was expected of next() if no input was set on parser");
        } catch(ex: XmlPullParserException) {}


        // check the simplest possible XML document - just one root element
        for(i in 1...3) {
            xpp.setInput(new StringInput(i == 1 ? "<foo/>" : "<foo></foo>"));
            var empty = (i == 1);
            Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
            xpp.next();
            Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "foo", null, empty, 0);
            xpp.next();
            Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "foo", null, false, -1);
            xpp.next();
            Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
        }

        // one step further - it has content ...


        xpp.setInput(new StringInput("<foo attrName='attrVal'>bar</foo>"));
        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 0, "", "foo", null, false, 1);
        Util.checkAttribNs(xpp, 0, null, "", "attrName", "attrVal");
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.TEXT, null, 0, null, null, "bar", false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 0, "", "foo", null, false, -1);
        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);


        xpp.setInput(new StringInput(
                         "<foo xmlns='n' xmlns:ns1='n1' xmlns:ns2='n2'>"+
                             "<ns1:bar xmlns:ns1='x1' xmlns:ns3='n3' xmlns='n1'>"+
                             "<ns2:gugu a1='v1' ns2:a2='v2' xml:lang='en' ns1:a3=\"v3\"/>"+
                             "<baz xmlns:ns1='y1'></baz>"+
                             "</ns1:bar></foo>"
                     ));

        Util.checkParserStateNs(xpp, 0, XmlEventType.START_DOCUMENT, null, 0, null, null, null, false, -1);

        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.START_TAG, null, 2, "n", "foo", null, false, 0);
        Assert.equals(0, xpp.getNamespaceCount(0));
        Assert.equals(2, xpp.getNamespaceCount(1));
        Util.checkNamespace(xpp, 0, "ns1", "n1", true);
        Util.checkNamespace(xpp, 1, "ns2", "n2", true);

        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.START_TAG, "ns1", 4, "x1", "bar", null, false, 0);
        Assert.equals(0, xpp.getNamespaceCount(0));
        Assert.equals(2, xpp.getNamespaceCount(1));
        Assert.equals(4, xpp.getNamespaceCount(2));
        Util.checkNamespace(xpp, 2, "ns1", "x1", true);
        Util.checkNamespace(xpp, 3, "ns3", "n3", true);

        xpp.next();
        Util.checkParserStateNs(xpp, 3, XmlEventType.START_TAG, "ns2", 4, "n2", "gugu", null, true, 4);
        Assert.equals(4, xpp.getNamespaceCount(2));
        Assert.equals(4, xpp.getNamespaceCount(3));
        Assert.equals("x1", xpp.getNamespaceByPrefix("ns1"));
        Assert.equals("n2", xpp.getNamespaceByPrefix("ns2"));
        Assert.equals("n3", xpp.getNamespaceByPrefix("ns3"));
        Util.checkAttribNs(xpp, 0, null, "", "a1", "v1");
        Util.checkAttribNs(xpp, 1, "ns2", "n2", "a2", "v2");
        Util.checkAttribNs(xpp, 2, "xml", "http://www.w3.org/XML/1998/namespace", "lang", "en");
        Util.checkAttribNs(xpp, 3, "ns1", "x1", "a3", "v3");

        xpp.next();
        Util.checkParserStateNs(xpp, 3, XmlEventType.END_TAG, "ns2", 4, "n2", "gugu", null, false, -1);

        xpp.next();
        Util.checkParserStateNs(xpp, 3, XmlEventType.START_TAG, null, 5, "n1", "baz", null, false, 0);
        Assert.equals(0, xpp.getNamespaceCount(0));
        Assert.equals(2, xpp.getNamespaceCount(1));
        Assert.equals(4, xpp.getNamespaceCount(2));
        Assert.equals(5, xpp.getNamespaceCount(3));
        Util.checkNamespace(xpp, 4, "ns1", "y1", true);
        Assert.equals("y1", xpp.getNamespaceByPrefix("ns1"));
        Assert.equals("n2", xpp.getNamespaceByPrefix("ns2"));
        Assert.equals("n3", xpp.getNamespaceByPrefix("ns3"));

        xpp.next();
        Util.checkParserStateNs(xpp, 3, XmlEventType.END_TAG, null, 5, "n1", "baz", null, false, -1);
        Assert.equals("y1", xpp.getNamespaceByPrefix("ns1"));
        Assert.equals("n2", xpp.getNamespaceByPrefix("ns2"));
        Assert.equals("n3", xpp.getNamespaceByPrefix("ns3"));

        xpp.next();
        Util.checkParserStateNs(xpp, 2, XmlEventType.END_TAG, "ns1", 4, "x1", "bar", null, false, -1);
        Assert.equals("x1", xpp.getNamespaceByPrefix("ns1"));
        Assert.equals("n2", xpp.getNamespaceByPrefix("ns2"));
        Assert.equals("n3", xpp.getNamespaceByPrefix("ns3"));

        xpp.next();
        Util.checkParserStateNs(xpp, 1, XmlEventType.END_TAG, null, 2, "n", "foo", null, false, -1);

        xpp.next();
        Util.checkParserStateNs(xpp, 0, XmlEventType.END_DOCUMENT, null, 0, null, null, null, false, -1);
        Assert.equals(null, xpp.getNamespaceByPrefix("ns1"));
        Assert.equals(null, xpp.getNamespaceByPrefix("ns2"));
        Assert.equals(null, xpp.getNamespaceByPrefix("ns3"));

    }

}

