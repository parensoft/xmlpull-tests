# XML Pull API tests adapted for the Haxe language.

This is an adaptation of Java's XML Pull API Test Suite (http://xmlpull.org/) to the Haxe language. 

These tests are LGPL-licensed.

The API itself is in the public domain, and is available from [Haxelib](http://lib.haxe.org/p/xmlpull).

